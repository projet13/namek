Membres du projet : 
* LE/Nam/28613397 
* BOUZOUAOUI/Anes/28616582

Origine de l'appelation du projet:
> Namek (ナメック星, Namekkusei, Planète Namek) est une planète du manga Dragon Ball, 
> ainsi que le nom du peuple qui y habite. 
> C’est la planète d’origine des Dragon Balls dont la quête est la trame principale de l’histoire.