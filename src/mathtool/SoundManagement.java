package mathtool;

import worlds.World;


import java.io.File;
import java.util.ArrayList;


public class SoundManagement { //SoundGesion
	private ArrayList<Track> effects;
    private boolean mute;

    public SoundManagement(){
        initSound();
    }


	public void initSound(){
        mute = false;
		effects = new ArrayList<Track>();
        File file = new File("audio/");
        String[] songs = file.list();
		for (String s : songs){
            effects.add(new Track("audio/"+s));
        }
		
	}

	public void playSound(String s) {
        if (mute) return;
        for (Track t : effects)
        	if (t.name.equals(s)) {
            	t.clip.start();
        	}
    }

    public void click() {
        if (mute) return;
        Track t = new Track("audio/click.wav");
        t.clip.start();
    }

    public void stopSound(String s) {
        for (Track t : effects)
        	if (t.name.equals(s)) {
            	t.clip.stop();
        	}
    }

    // Rename BackgroundMusic stating by 0
    public void toggleBackgroundMusic() {
        if (mute) return;
        Track t = effects.get(0);
        if (t.clip.isActive()) 
                    t.clip.stop();
        else 
            t.clip.start();
    }

    private boolean oldBackgroundMusic;

    public void mute() {
        if (mute == false){
            oldBackgroundMusic = effects.get(0).clip.isActive();
            // turn off all sounds
            for (Track t : effects)
                t.clip.stop();
        } else {
            if (oldBackgroundMusic)
                effects.get(0).clip.start();
            if (!World.morning())
                effects.get(1).clip.start();

        }
        mute = !mute;
    }
    
    public boolean isMute(){
        return mute == true ;
    }

}