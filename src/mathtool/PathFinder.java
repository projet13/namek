package mathtool;

public class PathFinder {
	// Soit deux objet A et B
	// pour que A go vers B : approach(A,B...)
	// pour que A fuit B 	: approach(B,A...) il suffit d inverser 
	public static int approach(int xa, int ya, int xb, int yb, int dx, int dy) {
		
		int right = (xb - xa + dx)%dx;
		int left = (xa - xb + dx)%dx;
		int down = (yb - ya + dy)%dy;
		int up = (ya - yb + dy)%dy;

		/* Codes of mvt :
		 	2
			^
		3 <-0-> 1
			v
			4
	 	*/
		
		if (down < up)
			return 4;
		if (up < down)
			return 2;
		if (right < left)
			return 1;
		if (left < right)
			return 3;
		return 0;
	}
	// distance entre A et B
	public static int distance(int xa, int ya, int xb, int yb, int dx, int dy) {
		return    Math.min(Math.abs(xa - xb + dx)%dx, Math.abs(xb - xa + dx)%dx) 
				+ Math.min(Math.abs(ya - yb + dy)%dy, Math.abs(yb - ya + dy)%dy);
	}
}
