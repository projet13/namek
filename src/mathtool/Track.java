package mathtool;

import java.io.File;

//the imports FOR TrackS
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Track {
	String name;
	Clip clip;
	
	public Track(String s) {
		this.name = s;
		try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(s).getAbsoluteFile());
            this.clip = AudioSystem.getClip();
            this.clip.open(audioInputStream);
        } catch(Exception ex) {
            System.out.println("Error with playing Track.");
            ex.printStackTrace();
        }
	}
}