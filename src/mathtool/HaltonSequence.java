package mathtool;

public class HaltonSequence {
	// get(index, base)
	public static double get(int i, int b) {
		double f = 1;
		double r = 0;
		
		while (i > 0) {
			f = f/b;
			r = r + f * (i % b);
			i = i/b;
		}
		
		return r;
	}
}
