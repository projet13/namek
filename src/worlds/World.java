// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12
package worlds;

import java.util.ArrayList;
import javax.media.opengl.GL2;

import cellularautomata.*;
import objects.*;
import mathtool.SoundManagement;


public abstract class World {
	protected static int iteration;
	protected static int dayCount;
	protected int season;	// 0-1-2-3 spring summer autumn winter
    // time gestion should be here for all the ecosystem
    private static  int DAY_LENGTH = 60; // a day is 60 seconds
    private static final int SEASON_LENGTH = 4; // a season lasts 4 days
    private static final float SUNRISE = 0;
    private static final float ANIMATION_TIME = 0.1f*DAY_LENGTH; //make it 0.2f*DAY for a longer sunset-sunrise //by default: 0.1f*DAY
    private static final float SUNSET = 0.6f*DAY_LENGTH; // make it 0.3f*DAY for a shorter day and longeur night 
    // by default: 0.7f*DAY (day should be longer than night for visibilty reasons for the presentation) 
    private final static int WIND = (int)(Math.random()*4);
    
    private static boolean rainyWeather;
    private static long TIME0;
    private int oldIt;
    private boolean switched;

    public SoundManagement sd;
    
   
    

	protected ArrayList<UniqueObject> uniqueObjects = new ArrayList<UniqueObject>();
	protected ArrayList<UniqueDynamicObject> uniqueDynamicObjects = new ArrayList<UniqueDynamicObject>();
    
	public ArrayList<UniqueDynamicObject> getUniqueDynamicObjects() {
		return uniqueDynamicObjects;
	}
	
	protected int dxCA;
	protected int dyCA;

	protected int indexCA;

	//protected CellularAutomataInteger cellularAutomata; // TO BE DEFINED IN CHILDREN CLASSES
    
	protected CellularAutomataDouble cellsHeightValuesCA;
	protected CellularAutomataDouble cellsHeightAmplitudeCA;
	
	public CellularAutomataColor cellsColorValues;

	private double maxEverHeightValue = Double.NEGATIVE_INFINITY;
	private double minEverHeightValue = Double.POSITIVE_INFINITY;

    public World( )
    {
    	// ... cf. init() for initialization
    }
    
    public void init( int __dxCA, int __dyCA, double[][] landscape )
    {
        TIME0 = System.currentTimeMillis()/1000 ;
        sd = new SoundManagement();
        sd.playSound("audio/0_BackgroundMusic.wav");
    	dxCA = __dxCA;
    	dyCA = __dyCA;
    	
    	iteration = 0;
    	dayCount = 1;
    	rainyWeather = false;
    	oldIt = -1;
    	season = 0;

    	this.cellsHeightValuesCA = new CellularAutomataDouble (__dxCA,__dyCA,false);
    	this.cellsHeightAmplitudeCA = new CellularAutomataDouble (__dxCA,__dyCA,false);
    	
    	this.cellsColorValues = new CellularAutomataColor(__dxCA,__dyCA,false);
    	
    	// init altitude and color related information
    	for ( int x = 0 ; x != dxCA ; x++ )
    		for ( int y = 0 ; y != dyCA ; y++ )
    		{
    			// compute height values (and amplitude) from the landscape for this CA cell 
    			double minHeightValue = Math.min(Math.min(landscape[x][y],landscape[x+1][y]),Math.min(landscape[x][y+1],landscape[x+1][y+1]));
    			double maxHeightValue = Math.max(Math.max(landscape[x][y],landscape[x+1][y]),Math.max(landscape[x][y+1],landscape[x+1][y+1])); 
    			
    			if ( this.maxEverHeightValue < maxHeightValue )
    				this.maxEverHeightValue = maxHeightValue;
    			if ( this.minEverHeightValue > minHeightValue )
    				this.minEverHeightValue = minHeightValue;
    			
    			cellsHeightAmplitudeCA.setCellState(x,y,maxHeightValue-minHeightValue);
    			cellsHeightValuesCA.setCellState(x,y,(minHeightValue+maxHeightValue)/2.0);
    		}
    	
    	initCellularAutomata(__dxCA,__dyCA,landscape);

    }
    
    public void step() 
    {

    	stepCellularAutomata();
    	stepAgents();
        stepObj();
    	iteration++;
        if ( getDay24Time() > 0 && getDay24Time() < 3 && !switched){  
            dayCount++;
            switched = true;
        }

        
        if (getDay24Time() > 3 && getDay24Time() < 6) // to fix many increament day bug
            switched = false;

        if (getDayTime() == SUNRISE){	// good morning!
            sd.playSound("audio/coque.wav"); 
            sd.stopSound("audio/1_night.wav"); 
        }
        if (getDayTime() == SUNSET + ANIMATION_TIME) 
                sd.playSound("audio/1_night.wav");
        if (getIteration() == 10 ) 
            sd.playSound("audio/2_birds.wav");

        // when rainyWeather begins
        if (!rainyWeather && getIteration() % 1600 == 0 ) {
            rainyWeather = true;
            sd.playSound("audio/rain.wav");
            oldIt = getIteration();
        }
        // rainyWeather last 1000 iterastion
        if (rainyWeather && getIteration() == oldIt+1000 )
            sd.stopSound("audio/rain.wav");
            rainyWeather = false;
    }
    
    public boolean isRainyWeather(){
        return rainyWeather == true;
    }
    
    public static int getIteration()
    {
    	return iteration;
    }
    
    abstract protected void stepAgents();

    abstract protected void stepObj();
    
    // ----

    protected abstract void initCellularAutomata(int __dxCA, int __dyCA, double[][] landscape);
    
    protected abstract void stepCellularAutomata();
    
    // ---
    
    abstract public int getForestCellValue(int x, int y); // used by the visualization code to call specific object display.

    abstract public void setForestCellValue(int x, int y, int state);
    
    abstract public double getTemperatureCellValue(int x, int y);
    
    abstract public void setTemperatureCellValue(int x, int y, double temperature);
    
    abstract public float[] coloring(float altitude, float temp);
    
    abstract public void tempCtrl(boolean b, float amount);
    
    abstract public void toggleGoogle();
    
    // ---- 
    
    public double getCellHeight(int x, int y) // used by the visualization code to set correct height values
    {
    	return cellsHeightValuesCA.getCellState(x%dxCA,y%dyCA);
    }
    
    // ---- 
    
    public float[] getCellColorValue(int x, int y) // used to display cell color
    {
    	float[] cellColor = this.cellsColorValues.getCellState( x%this.dxCA , y%this.dyCA );

    	float[] color  = {cellColor[0],cellColor[1],cellColor[2],1.0f};
        
        return color;
    }

	abstract public void displayObjectAt(World _myWorld, GL2 gl, int cellState, int x,
			int y, double height, float offset,
			float stepX, float stepY, float lenX, float lenY,
			float normalizeHeight); 

	public void displayUniqueObjects(World _myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset,
			float stepX, float stepY, float lenX, float lenY, float normalizeHeight) 
	{
    	for ( int i = 0 ; i < uniqueObjects.size(); i++ )
    		uniqueObjects.get(i).displayUniqueObject(_myWorld,gl,offsetCA_x,offsetCA_y,offset,stepX,stepY,lenX,lenY,normalizeHeight);
    	for ( int i = 0 ; i < uniqueDynamicObjects.size(); i++ )
    		uniqueDynamicObjects.get(i).displayUniqueObject(_myWorld,gl,offsetCA_x,offsetCA_y,offset,stepX,stepY,lenX,lenY,normalizeHeight);
	}
    
	public int getWidth() { return dxCA; }
	public int getHeight() { return dxCA; }

	public double getMaxEverHeight() { return this.maxEverHeightValue; }
	public double getMinEverHeight() { return this.minEverHeightValue; }

    // Time Gestion
    public static boolean morning(){
        return getDayTime() >= getSunrise() + getAnimationTime() && getDayTime() <= getSunset() + getAnimationTime();
    }
    //Getters
    public static float getAnimationTime() {
        return ANIMATION_TIME;
    }

    public static float getSunrise() {
        return SUNRISE;
    }

    public static int getDayLength() {
        return DAY_LENGTH;
    }

	public static float getSunset() {
		return SUNSET;
	}

    public static int getDayTime() {
        return getAbsoluteTime() % DAY_LENGTH ;
    }

    public static int getDay24Time() {
        return (getDayTime()*24/DAY_LENGTH + 6) % 24;
    }

	/**
	 * @return the wind
	 */
	public static int getWind() {
		return WIND;
	}

	public static int getDayCount() {
		return dayCount;
	}
	
	public static int getSeason() {	// 0-2-4-6 spring summer autumn winter
		int dayOfYear = (getDayCount() % (SEASON_LENGTH * 4));
		return dayOfYear / (SEASON_LENGTH / 2);
	}

    public static String getSeasonName(){
        String s = "NoSeason";
        switch (getSeason()) {
            case 0: case 1: s="Spring"; break;
            case 2: case 3: s="Summer"; break;
            case 4: case 5: s="Autumn"; break;
            case 6: case 7: s="Winter"; break;
        }
		return s;
    }

    public static long getDayTime0() {
        return TIME0;
    }
	
    public static int getAbsoluteTime() {
        return (int)(System.currentTimeMillis()/1000 % TIME0);
    }

    public static void plusDay(){
        dayCount++;  
    }

    public static void minusDay(){
        dayCount--; 
    }

}
