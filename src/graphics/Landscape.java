package graphics;

import worlds.*;
import applications.simpleworld.TemperatureCA;

import java.awt.*;
import java.awt.event.*;
import javax.media.opengl.*;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.*;
import com.jogamp.opengl.util.Animator;
import com.jogamp.opengl.util.gl2.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import landscapegenerator.LoadFromFileLandscape;
import landscapegenerator.PerlinNoiseLandscapeGenerator;



public class Landscape implements GLEventListener, KeyListener, MouseListener{
		private World _myWorld; 
		private static GLCapabilities caps;  // GO FAST ???
		static boolean MY_LIGHT_RENDERING = false; // true: nicer but slower
		final static boolean SMOOTH_AT_BORDER = true; // nicer (but wrong) rendering at border (smooth altitudes)
		//final static double landscapeAltitudeRatio = 0.6; // 0.5: half mountain, half water ; 0.3: fewer water
		static boolean VIEW_FROM_ABOVE = false; // also deactivate altitudes
		static boolean DISPLAY_OBJECTS = true; // useful to deactivate if view_from_above
		final static boolean DISPLAY_FPS = true; // on-screen display
        static boolean DISPLAY_MENU = false; // on-screen display
        final static int LAYOUT_X = 1024;
        final static int LAYOUT_Y = 768;
        static boolean DONE = false; // for welcome message in the begining

		/*
		 * benchmarking 
		 * 		Airbook (w/wo visible display) : 
		 * 			true:  frame per second  : frames per second  : 59 ; polygons per second: 966656 --- frames per second  : 82 ; polygons per second: 1343488
		 * 			false: frames per second  : 61 ; polygons per second: 999424 --- frames per second  : 254 ; polygons per second: 4161536 (!!!)
		 * 
		 * 
		 * Bonnes pratiques:
		 * - gl.Begin() ... gl.glEnd(); : faire un minimum d'appel, idealement un par iteration. (gain de 50% a 100% ici)
		 * - gl.glCullFace(GL.GL_FRONT); ... gl.glEnable(GL.GL_CULL_FACE); : si la scene le permet, reduit le nb de polyg a afficher.
		 * - TRIANGLE SPLIT permet de reduire le nombre d'appels a OpenGL (gl.begin et end)
		 * - each call to gl.glColor3f costs a lot (speed is down by two if no call!)
		 */
	
		static Animator animator; 
		//  https://sites.google.com/site/justinscsstuff/jogl-tutorial-3
		//  if you use a regular Animator object instead of the FPSAnimator, your program will render as fast as possible. You can, however, limit the framerate of a regular Animator by asking the graphics driver to synchronize with the refresh rate of the display (v-sync). Because the target framerate is often the same as the refresh rate, which is often 60-75 or so, this method is a great choice as it lets the driver do the work of limiting frame changes. 
		//  However, some Intel GPUs may ignore this setting. In the init method, active v-sync as follows:
		//  add : drawable.getGL().setSwapInterval(1); in the init method
		//  then: in the main method, replace the FPSAnimator with a regular Animator.
		
		//private float rotateX = 0.0f;


        /**************************** by ME **********************/
        /**** variable   *********************************************/
		
        private float ZOOM = -155.0f; // -500 
        private float ABOVE_ROT = -10.0f; // become -90 
        private float ABOVE_Z_ROT = 0.0f;
        private boolean CREDITS = false;
        private boolean animation = false;

        float pos;
        String message;
        float var_toPtint;

        private void byDefault(){
            if (VIEW_FROM_ABOVE) {
                ZOOM = -500.0f;
                ABOVE_ROT = 0.0f;
                ABOVE_Z_ROT = 0.0f;
            } else {
                ZOOM = -200.0f;
                ABOVE_ROT = -10.0f;
                ABOVE_Z_ROT = 0.0f;
            }
        }


        /*********************************************************/
        /*********************************************************/


        int it = 0;
        int movingIt = 0;
        int dxView;
        int dyView;

        double[][] landscape; 

        int lastFpsValue = 0;
        
        public static int lastItStamp = 0;
        public static long lastTimeStamp = 0;
        
        // visualization parameters
        
    	float heightFactor; //64.0f; // was: 32.0f;
        double heightBooster; // applied to landscape values. increase heights.
        // -- NOTE that this could also be achieved using heighFactor but is decomposed to enable further pre-calc of height values
        // heightFactor deals with visualization
        // heigBooster will impact landscape array content 
       
		float offset;
		float stepX;
		float stepY;
		float lenX;
		float lenY;
		
        float smoothFactor[];
        int smoothingDistanceThreshold;
        
        int movingX = 0; 
        int movingY = 0; 
        
        /**
         * 
         */
        public Landscape (World __myWorld, int __dx, int __dy, double scaling, double landscapeAltitudeRatio)
        {
    		_myWorld = __myWorld;

    		landscape = PerlinNoiseLandscapeGenerator.generatePerlinNoiseLandscape(__dx,__dy,scaling,landscapeAltitudeRatio,8); // 2^8=256 is enough
    		
    		initLandscape();
        }

        /**
         * 
         */
        public Landscape (World __myWorld, String __filename, double scaling, double landscapeAltitudeRatio)
        {
    		_myWorld = __myWorld;

    		landscape = LoadFromFileLandscape.load(__filename,scaling,landscapeAltitudeRatio);

    		initLandscape();
        }
        
        /**
         * 
         */
        private void initLandscape()
        {
       		dxView = landscape.length;
    		dyView = landscape[0].length;

    		System.out.println("Landscape contains " + dxView*dyView + " tiles. (" + dxView + "x" + dyView +")");
        	
    		_myWorld.init(dxView-1,dyView-1,landscape);
    		
    		heightFactor = 32.0f; //64.0f; // ->>>>was: 32.0f;
            heightBooster = 6.0; // default: 2.0 // 6.0 makes nice high mountains.->>>>was: 6.0f;
           
    		offset = -200.0f; // was: -40. // ->>>>was: -200.0f;
    		stepX = (-offset*2.0f) / dxView;
    		stepY = (-offset*2.0f) / dxView;
    		lenX = stepX / 2f;
    		lenY = stepY / 2f;
    		
            smoothFactor = new float[4];
            for ( int i = 0 ; i < 4 ; i++ )
            	smoothFactor[i] = 1.0f;
            
            smoothingDistanceThreshold = 30; //30;
            
        }
        
        /**
         * 
         */
        public static Landscape run(Landscape __landscape)
        {
    		caps = new GLCapabilities(null); //!n
    		caps.setDoubleBuffered(true);  //!n
    		
    		final GLCanvas canvas = new GLCanvas(caps); // original
    		
            final Frame frame = new Frame("Namek"); // Change this name for my version of the project
            animator = new Animator(canvas);
            //Landscape myLandscape = new Landscape(dx,dy,myWorld);
            canvas.addGLEventListener(__landscape);
            canvas.addMouseListener(__landscape);// register mouse callback functions
            canvas.addKeyListener(__landscape);// register keyboard callback functions
            frame.add(canvas);
            frame.setSize(LAYOUT_X, LAYOUT_Y); // ---->> SIZE of the layout frame.setSize(1024, 768)
            //frame.setSize(1280, 960);
            frame.setResizable(true); // was false
            frame.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                            animator.stop();
                            frame.dispose();
                            System.exit(0);
                    }
            });
            frame.setVisible(true);
            //animator.setRunAsFastAsPossible(true); // GO FAST!  --- DOES It WORK? 
            animator.start();
            canvas.requestFocus();
            
            return __landscape;
        }
        
        

        /**
         * OpenGL Init method
         */
        //@Override
        public void init(GLAutoDrawable glDrawable) {
                GL2 gl = glDrawable.getGL().getGL2();

                // Enable front face culling (can speed up code, but is not always 
                // GO FAST ???
                
                // double buffer
                gl.glEnable(GL2.GL_DOUBLEBUFFER);
                glDrawable.setAutoSwapBufferMode(true);
                
                // Enable VSync
                // ? gl.setSwapInterval(1);
                // END of GO FAST ???


                gl.glShadeModel(GLLightingFunc.GL_SMOOTH);
                // was gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                if (World.morning() ) { //blue ski rgb(0.53,0.81,0.92) /// the sky
                    gl.glClearColor(0.53f, 0.81f, 0.92f, 0.0f);
                } else { 
                    gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                }
                

                gl.glClearDepth(1.0f); // was 1.0f
                gl.glEnable(GL.GL_DEPTH_TEST);
                gl.glDepthFunc(GL.GL_LEQUAL);
                gl.glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
                
                // Culling - display only triangles facing the screen
                gl.glCullFace(GL.GL_FRONT);
                gl.glEnable(GL.GL_CULL_FACE);

                // trucs d'alex
                gl.glEnable(GL.GL_DITHER);
                
                
        }

        
        
        /**
         * 
         */
        //@Override
        public void display(GLAutoDrawable gLDrawable) {
        		final GL2 gl = gLDrawable.getGL().getGL2();
                //**********************************

                //**********************************
        		// ** compute FPS
        		
        		if ( System.currentTimeMillis() - lastTimeStamp >= 1000 )
        		{
    				int fps = ( it - lastItStamp ) / 1;
       			
        			if ( Math.random() < 0.10 ) // display in console every ~10 updates
        			{
	        			//System.out.print("frames per second  : "+fps+" ; ");
        			}

        			

        			lastItStamp = it;
        			lastTimeStamp = System.currentTimeMillis();
        			
        			lastFpsValue = fps;
        		}

        		
        		// ** clean screen
        		
                /*
                // DAY-NIGHT SKY
                if (_myWorld.morning() ) { //blue sky rgb(0.53,0.81,0.92)
                        gl.glClearColor(0.53f, 0.81f, 0.92f, 0.0f);
                    } else { //it's night 
                        gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
                    }
                 */
        		float time = World.getDayTime();
        		float sunrise = World.getSunrise();
        		float sunset = World.getSunset();
        		float delay = World.getAnimationTime();
           		float[] color_ = new float[3];
                if (time >= sunrise && time <= sunrise + delay) {
                	//sunrise
                	float step = (time - sunrise) / delay;
                    color_[0] = (float) 0.511f * step + 0.019f;
                    color_[1] = (float) 0.791f * step + 0.019f;
                    color_[2] = (float) 0.834f * step + 0.086f;
                } else if ( time <= World.getSunset() ) {
                    // day time, (0.53,0.81,0.92)
                    color_[0] = 0.53f;
                    color_[1] = 0.81f;
                    color_[2] = 0.92f;
                } else if ( time <= World.getSunset() + World.getAnimationTime() ) {
                	// sunset
                	float step = (time - sunset) / delay;
                    color_[0] = (float) - 0.511f * step + 0.53f;
                    color_[1] = (float) - 0.791f * step + 0.81f;
                    color_[2] = (float) - 0.834f * step + 0.92f;
                } else {
                    // night time
                    color_[0] = 0.019f;
                    color_[1] = 0.019f;
                    color_[2] = 0.086f;
                }
                gl.glClearColor(color_[0],color_[1], color_[2], 0.0f);

                gl.glClear(GL.GL_COLOR_BUFFER_BIT);
                gl.glClear(GL.GL_DEPTH_BUFFER_BIT);
                gl.glLoadIdentity();


                // Message welcome 

                if ( ABOVE_ROT > -88 && !DONE) // last 2 second
                {
                    gl.glPushMatrix();
                    gl.glColor3f(1,1,1);
                    GLUT glut = new GLUT();
                    gl.glTranslatef(0, 0, 0);
                    gl.glWindowPos2d(LAYOUT_X/2.5, LAYOUT_Y/2);// 0, 728
                    glut.glutBitmapString(GLUT.BITMAP_TIMES_ROMAN_24, "W E L C O M E ");
                    // increase to less wlecome time
                    ABOVE_ROT -= 1f;
                    gl.glPopMatrix();
                } else {
                	DONE= true;
                }

                // Press [F1] for menu
                if ( true ) 
                {
                    gl.glPushMatrix();
                    if (World.morning()) {
                        gl.glColor3f(0.42f,0.65f, 0.74f);
                    } else {
                        gl.glColor3f(0.1f,0.15f, 0.22f);
                    }
                    GLUT glut = new GLUT();
                    gl.glTranslatef(0, 0, 0);
                    gl.glWindowPos2d(LAYOUT_X/1.3, LAYOUT_Y/1.1);// 0, 728
                    glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18, "Press [F1] for menu");
                    gl.glPopMatrix();
                }

                

                
                if (true){ // display affff
                    ///////
                    pos = 0;
                    //////
                    gl.glPushMatrix();
                    gl.glColor3f(1f,0f, 0f);
                    gl.glWindowPos2d(50, LAYOUT_Y/1.1);// 0, 728
                    GLUT glut = new GLUT();
                    gl.glTranslatef(0, 0, 0);
                    glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18,
                        " * Run since: "+Integer.toString(World.getAbsoluteTime())+"s"+
                        " * DAY: "+Integer.toString(World.getDayCount())+
                        " * TIME: "+Integer.toString(World.getDay24Time())+"h"+
                        " * Season: "+World.getSeasonName()+
                        " * temperature: "+ Math.floor(TemperatureCA.getTemperatureWorld())
                        );
                    gl.glPopMatrix();   
                }
                /*
                if (false){ // display getTimeSpeed()
                    ///////
                    pos = 1;
                    message = "Zoome = ";
                    var_toPtint = ZOOM ;
                    //////
                    gl.glPushMatrix();
                    gl.glColor3f(1f,0f, 0f);
                    gl.glWindowPos2d(LAYOUT_X/7, LAYOUT_Y/1.3-pos*30);// 0, 728
                    GLUT glut = new GLUT();
                    gl.glTranslatef(0, 0, 0);
                    glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18,"****"+message+Float.toString(var_toPtint));
                    gl.glPopMatrix();   
                }*/


                // ** display FPS on screen
                if ( DISPLAY_FPS )
                {
                    gl.glPushMatrix();
                    gl.glColor3f((float)Math.random(),(float)Math.random(),(float)Math.random()); // do this before calling glWindowsPos2d
                    gl.glWindowPos2d(0, 728);// 0, 728
                    GLUT glut = new GLUT();
                    gl.glTranslatef(0, 0, 0);
                    glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18, "fps: " + lastFpsValue);
                    gl.glPopMatrix();
                }


                // DISPLAY MENU
                if ( DISPLAY_MENU )
                {
                    gl.glPushMatrix();
                    
                    
                    GLUT glut = new GLUT();
                    String tab[] = {" ",
                        "[F2] toggle Music\n",
                        "[F3] Mute\n",
                        "[cursor keys] navigate\n",
                        "[Z/S] move Up/Down",
                        "[Q-D] Rotation",
                        "[v] change to Above view\n",
                        "[o] objects display on/off\n",
                        "[c] heat google mode\n", 
                        "[K/L] -/+ DAY\n",
                        "[F9] byDefault View ",
                        "[F12] Credits ",
                        "[F1] Exit Menu ",
                        // Other
                        /*
                        "[1] decrease altitude booster\n",
                        "[2] increase altitude booster\n",
                        "[q/d] rotation wrt landscape\n",
                        "[z] increase heat gain\n",
                        "[x] increase heat loss\n",
                        */
                        };
                    String tab_above[] = {" ",
                        "[F2] toggle Music\n",
                        "[F3] Mute\n",
                        "[cursor keys] navigate\n",
                        "[+/-] Zoom In/Out",
                        "[Q/D-Z/S] Rotation ",
                        "[v] change to land view\n",
                        "[o] objects display on/off\n",
                        "[c] heat google mode\n", 
                        "[K/L] -/+ DAY\n",
                        "[F9] byDefault View ",
                        "[F12] Credits ",
                        "[F1] Exit Menu ",
                        // Other
                        /*
                        "[o] objects display on/off\n",
                        "[1] decrease altitude booster\n",
                        "[2] increase altitude booster\n",
                        "[q/d] rotation wrt landscape\n",
                        "[z] increase heat gain\n",
                        "[x] increase heat loss\n",
                        "[c] heat google mode\n" */
                        };

                    gl.glTranslatef(50, 0, 0);
                    if (VIEW_FROM_ABOVE) { // Menu Above View
                        gl.glColor3f(1,0,0);
                        gl.glWindowPos2d(LAYOUT_X/1.28, LAYOUT_Y/1.2);// 0, 728
                        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18,"Menu Above View");
                        if ( ! (time > World.getSunset()) ) 
                            gl.glColor3f(0,0,0);
                        else 
                            gl.glColor3f(1,1,1);
                        for (int i =0 ; i < tab_above.length  ; i ++ ) {
                            gl.glWindowPos2d(LAYOUT_X/1.28, LAYOUT_Y/1.2 - i*30);// 0, 728
                            glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18,tab_above[i]);
                        }
                    } else { // Menu Land View
                        gl.glColor3f(1,0,0);
                        gl.glWindowPos2d(LAYOUT_X/1.28, LAYOUT_Y/1.2);// 0, 728
                        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18,"Menu Land View");
                        if ( ! (time > World.getSunset()) ) 
                            gl.glColor3f(0,0,0);
                        else 
                            gl.glColor3f(1,1,1);
                        for (int i =0 ; i < tab.length  ; i ++ ) {
                            gl.glWindowPos2d(LAYOUT_X/1.28, LAYOUT_Y/1.2 - i*30);// 0, 728
                            glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18,tab[i]);
                        }
                    }


                    if (CREDITS == true) {
                        gl.glWindowPos2d(20, 35);
                        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "Anes BOUZOUAI & Nam LEE");
                        gl.glWindowPos2d(20, 20);
                        glut.glutBitmapString(GLUT.BITMAP_HELVETICA_10, "SOROBONNE UNIVERSITY 2020");
                    }
                    
                    gl.glPopMatrix();
                }

                // display MUTE   icon 
                if ( _myWorld.sd.isMute() )
                {
                    gl.glPushMatrix();
                    gl.glColor3f(1,0,0);
                    GLUT glut = new GLUT();
                    gl.glTranslatef(0, 0, 0);
                    gl.glWindowPos2d(LAYOUT_X/7, LAYOUT_Y/1.3);// 0, 728
                    glut.glutBitmapString(GLUT.BITMAP_HELVETICA_18," MUTE ");
                    gl.glPopMatrix();
                }
        	
                // ** render all
               
                // *** ADD LIGHT
               
                if ( MY_LIGHT_RENDERING )
                {
	            	// Prepare light parameters.
	                float SHINE_ALL_DIRECTIONS = 1; // was 1
	                //float[] lightPos = {120.f, 120.f, -200.f, SHINE_ALL_DIRECTIONS};
	                //float[] lightPos = {40.f, 0.f, -300.f, SHINE_ALL_DIRECTIONS};
	                float[] lightPos = {0.f, 40.f, -100.f, SHINE_ALL_DIRECTIONS}; // -100
	                //float[] lightColorAmbient = {0.2f, 0.2f, 0.2f, 1f};
	                float[] lightColorAmbient = {0.5f, 0.5f, 0.5f, 1f};
	                float[] lightColorSpecular = {0.8f, 0.8f, 0.8f, 1f};
	
	                // Set light parameters.
	                
	                gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, lightPos, 0);
	                gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, lightColorAmbient, 0);
	                gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, lightColorSpecular, 0);
	                
	                // Enable lighting in GL.
	                gl.glEnable(GL2.GL_LIGHT1);
	                gl.glEnable(GL2.GL_LIGHTING);
                }
                

                // FIERTE !!!!!!! make animation once change view !
                if ( VIEW_FROM_ABOVE){
                    // je passe de landView a aboveView
                    if (animation) {
                        if (ABOVE_ROT < 0)  ABOVE_ROT += 3;
                        if (ZOOM > -500) ZOOM -= 10;
                        if (ABOVE_ROT >= 0 && ZOOM <= -500) animation = false;
                    }
                	// as seen from above
                	gl.glTranslatef(0.0f, 0.0f, ZOOM); // 0,0,-500 ZOOM FROM ABOVE
                    gl.glRotatef(ABOVE_ROT, 1.0f, 0.0f, 0.0f); //(rotateX, 0.0f, 1.0f, 0.0f);
                    gl.glRotatef(ABOVE_Z_ROT, 0.0f, 0.0f, 1.0f);
                    // display  TEST 
                    
                } else {
                    if (animation) {
                        //if (ABOVE_ROT < 0)  ABOVE_ROT += 3;
                        if (ZOOM < -165) ZOOM += 15;
                        if (ABOVE_ROT > -81)  ABOVE_ROT -= 3;
                        // as seen from above
                        gl.glTranslatef(0, 0 , ZOOM); // 0,0,-500 ZOOM FROM ABOVE
                        gl.glRotatef(ABOVE_ROT, 1.0f, 0.0f, 0.0f); //(rotateX, 0.0f, 1.0f, 0.0f);
                        gl.glRotatef(ABOVE_Z_ROT, 0.0f, 0.0f, 1.0f);
                        if (ZOOM >= -165 && ABOVE_ROT <= -81 ) animation = false;
                    } else {
                    	//ZOOM = -44;
                    	//ABOVE_ROT = -81;
                        gl.glTranslatef(0.0f, ZOOM + 121 , -130 );
                        gl.glRotatef(ABOVE_ROT, 1.0f, 0.0f, 0.0f); 
                        gl.glRotatef(ABOVE_Z_ROT, 0.0f, 0.0f, 1.0f);
                    }
                }

                it++;

				
				_myWorld.step();

        		// ** draw everything

            	gl.glBegin(GL2.GL_QUADS);                
                
                //movingX = movingIt;// it; // was: it
                //movingY = 0; // was: it
                
                for ( int x = 0 ; x < dxView-1 ; x++ )
                	for ( int y = 0 ; y < dyView-1 ; y++ )
                	{
           			 	
		                double height = _myWorld.getCellHeight(x+movingX,y+movingY);
           			 	int cellState = _myWorld.getForestCellValue(x+movingX,y+movingY);	
           			 	float[] color = _myWorld.getCellColorValue(x+movingX,y+movingY);

	                	// compute CA-based coloring

                        gl.glColor3f(color[0],color[1],color[2]);
                        
                        // * if light is on
	                	if ( MY_LIGHT_RENDERING )
                        {
	                        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_AMBIENT, color, 0 );
	                        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_DIFFUSE, color, 0 );
	                        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_SPECULAR, color, 0 );
	                        gl.glMateriali( GL.GL_FRONT_AND_BACK, GL2.GL_SHININESS, 4 );
	                        float[] colorBlack  = {0.0f,0.0f,0.0f,1.0f};
	                        gl.glMaterialfv( GL.GL_FRONT_AND_BACK, GL2.GL_EMISSION, colorBlack, 0 );
                        }
                        
                        // Border visual smoothing : smooth altitudes near border (i.e. nicer rendering)
                        if ( SMOOTH_AT_BORDER && VIEW_FROM_ABOVE != true)
                        {
	                        if ( Math.min(Math.min(x, dxView-x-1),Math.min(y, dyView-y-1)) < smoothingDistanceThreshold )
	                        {

	                            for ( int i = 0 ; i < 4 ; i++ )
	                            {
	                            	int xIt = i==1||i==2?1:0;
	                            	int yIt = i==0||i==1?1:0;
	                            	//float xSign = i==1||i==2?1.f:-1.f;  WTF is that ?
	                            	//float ySign = i==0||i==1?1.f:-1.f;
	                            	
		                        	smoothFactor[i] = (float)
		                        			Math.min(
		                        					Math.min( 1.0 , (double)Math.min(x+xIt,dxView-x+xIt)/(double)smoothingDistanceThreshold ) ,  // check x-axis
		                        					Math.min( 1.0 , (double)Math.min(y+yIt,dyView-y+yIt)/(double)smoothingDistanceThreshold )    // check y-axis
		                        					);
	                            }	                            	
	                        }
	                        else
	                        {
	                        	for ( int i = 0 ; i < 4 ; i++ )
	                        		smoothFactor[i] = 1.0f;
	                        }
                        }

                        // use dxCA instead of dxView to keep synchronization with CA states
                        
                        for ( int i = 0 ; i < 4 ; i++ )
                        {
                        	int xIt = i==1||i==2?1:0;
                        	int yIt = i==0||i==1?1:0;
                        	float xSign = i==1||i==2?1.f:-1.f;
                        	float ySign = i==0||i==1?1.f:-1.f;
                        	
                            float zValue = 0.f;

                        	if ( VIEW_FROM_ABOVE == false )
	                        {
	                        	//double altitude = landscape[(x+xIt+movingX)%(dxCA][(y+yIt+movingY)%dyCA] * heightBooster;
	                        	double altitude = landscape[(x+xIt+movingX)%(dxView-1)][(y+yIt+movingY)%(dyView-1)] * heightBooster;
	                        	if ( altitude < 0 ) 
	                        		zValue = 0;
	                        	else
	                        		zValue = heightFactor*(float)altitude * smoothFactor[i];
	                        }
	                        
	                        gl.glVertex3f( offset+x*stepX+xSign*lenX, offset+y*stepY+ySign*lenY, zValue);
                        }

                        /**/
                        
                        // * display objects

                        // TODO+: diplayObjectAt(x,y,...) ==> on y gagne quoi? les smoothFactors. C'est tout. Donc on externalise?
                        
                        if ( DISPLAY_OBJECTS == true) // calls my world with the enough info to display anything at this location.
                        {
                        	float normalizeHeight = ( smoothFactor[0] + smoothFactor[1] + smoothFactor[2] + smoothFactor[3] ) / 4.f * (float)heightBooster * heightFactor;
                        	_myWorld.displayObjectAt(_myWorld,gl,cellState, x, y, height, offset, stepX, stepY, lenX, lenY, normalizeHeight);
                        }
                	}
	            
	            /**/
	            
	            // TODO+: displayObjects()
	            
	            if ( DISPLAY_OBJECTS == true) // calls my world with enough info to display anything anywhere
                { 
	            	float normalizeHeight = (float)heightBooster * heightFactor;
	            	_myWorld.displayUniqueObjects(_myWorld,gl,movingX,movingY,offset,stepX,stepY,lenX,lenY,normalizeHeight); 
	            }

	            gl.glEnd();      		

                // increasing rotation for the next iteration    
  
                //gl.glFlush(); // GO FAST ???
            	//gLDrawable.swapBuffers(); // GO FAST ???  // should be done at the end (http://stackoverflow.com/questions/1540928/jogl-double-buffering)
            	

        }
        

        
 
        
        /**
         * 
         */
        //@Override
        public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width, int height) {
        		if ( this.it == 0 )
		        	System.out.println( "W"+"o"+"r"+"l"+"d"+" "+"O"+
		            "f"+" "+"C"+"e"+"l"+"l"+"s"+" "+"-"+" "+"n"+"i"+
		            "c"+"o"+"l"+"a"+"s"+"."+"b"+"r"+"e"+"d"+"e"+"c"+
		            "h"+"e"+(char)(0x40)+"u"+"p"+"m"+"c"+"."+"f"+"r"+
		            ","+" "+"2"+"0"+"1"+"3"+"\n");
        		GL2 gl = gLDrawable.getGL().getGL2();
                final float aspect = (float) width / (float) height;
                gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
                gl.glLoadIdentity();
                final float fh = 0.5f;
                final float fw = fh * aspect;
                gl.glFrustumf(-fw, fw, -fh, fh, 1.0f, 1000.0f);
                gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
                gl.glLoadIdentity();
        }
 
        
        /**
         * 
         */
        //@Override
        public void dispose(GLAutoDrawable gLDrawable) {
        }
 

      
        
        /**
         * 
         * @param args
         */
        /*
		public static void main(String[] args) {

        	initLandscape(200,200, new WorldOfTrees());

        }
        */


		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		@Override
		  public void mousePressed(MouseEvent mouse)
		  {
			/* example from doublebuf.java
		    switch (mouse.getButton()) {
		      case MouseEvent.BUTTON1:
		        spinDelta = 2f;
		        break;
		      case MouseEvent.BUTTON2:
		      case MouseEvent.BUTTON3:
		        spinDelta = 0f;
		        break;
		    }
		    /**/
		  }

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

        //ssss
		@Override
		public void keyPressed(KeyEvent key) {
			switch (key.getKeyCode()) {
			case KeyEvent.VK_ESCAPE:
				new Thread()
				{
					public void run() { animator.stop();}
				}.start();
				System.exit(0);
				break;
            // turn off music
            case KeyEvent.VK_F2:
                _myWorld.sd.toggleBackgroundMusic();
                break;
            // Zoome in (prend en charge US and Frensh KEyboard)
            case KeyEvent.VK_S : 
                if (VIEW_FROM_ABOVE) {
                    if (ABOVE_ROT >= -90 ) ABOVE_ROT -= 5;
                } else {
                    if (ZOOM < -70 ) ZOOM += 5;
                }
                break;
            // Zoome out
            case KeyEvent.VK_Z :
                if (VIEW_FROM_ABOVE) {
                    if (ABOVE_ROT < -0 ) ABOVE_ROT += 5;
                } else {
                    ZOOM -= 5;
                }
                break;
            // rotation suivant Y (Titl)
			case KeyEvent.VK_Q:
                ABOVE_Z_ROT -= 5;
				break;
			case KeyEvent.VK_D:
                ABOVE_Z_ROT += 5;
				break;
            // vuciel
			case KeyEvent.VK_V:
                if (! VIEW_FROM_ABOVE) {
                    ABOVE_ROT = -90.0f;
                    ZOOM = -200.0f;
                }
                animation = true;
				VIEW_FROM_ABOVE = !VIEW_FROM_ABOVE ;
				break;
            // accelerateTime
            case KeyEvent.VK_L:
                _myWorld.sd.click();
                World.plusDay();
                break;
            case KeyEvent.VK_K:
                _myWorld.sd.click();
                World.minusDay();
                break;
            /*//**************************************************************/
            // rotation en continue
            // rotation suivant Y (Titl)
            /*
            case KeyEvent.VK_Q:
                _myWorld.sd.playSound("audio/click.wav");
                if (!VIEW_FROM_ABOVE) {
                    Y_ROT-= 5;
                } else {
                    ABOVE_Z_ROT-= 5; // ROTAION ABOVE
                }
                break;
            case KeyEvent.VK_D:
                _myWorld.sd.playSound("audio/click.wav");
                if (!VIEW_FROM_ABOVE) {
                    Y_ROT+= 5;
                } else {
                    ABOVE_Z_ROT+= 5; // ROTAION ABOVE
                }
                break;
            */
            /*//**************************************************************/

            // ABOVE zoom in 
            case KeyEvent.VK_ADD : case KeyEvent.VK_PLUS :
                ZOOM += 15 ;
                break;
            // above zoom out
            case KeyEvent.VK_SUBTRACT : case KeyEvent.VK_MINUS :
                ZOOM -= 15;
                break; 
            
            // re initialisation of the view
            case KeyEvent.VK_F9:
                _myWorld.sd.click();
                byDefault();
                break;
            // mute sound
            case KeyEvent.VK_F3:
				_myWorld.sd.playSound("audio/click.wav");
                _myWorld.sd.mute();
                break;
            // menu dysplay
            case KeyEvent.VK_F1:
                _myWorld.sd.click();
                DISPLAY_MENU = !DISPLAY_MENU ;
                break;
            // CREDITS
            case KeyEvent.VK_F12:
                _myWorld.sd.click();
                CREDITS = !CREDITS ;
                break;
			case KeyEvent.VK_R:
                //MY_LIGHT_RENDERING = !MY_LIGHT_RENDERING;
				break;
            // display object
			case KeyEvent.VK_O:
                DISPLAY_OBJECTS = !DISPLAY_OBJECTS;
				break;
                /*
            // rotation suivant Y (Titl)
            /*
			case KeyEvent.VK_NUMPAD1: case KeyEvent.VK_1:  // Change montagns echelle
				_myWorld.sd.playSound("audio/click.wav");
                _myWorld.sd.playSound("audio/click.wav");heightBooster++;

				break;
			case KeyEvent.VK_NUMPAD2 : case KeyEvent.VK_2:
				_myWorld.sd.playSound("audio/click.wav");
                _myWorld.sd.playSound("audio/click.wav");if ( heightBooster > 0 )

					heightBooster--;
				break;
            */
			case KeyEvent.VK_UP:
                movingY = ( movingY + 1 ) % (dyView-1);
				break;
			case KeyEvent.VK_DOWN:
                movingY = ( movingY - 1 + dyView-1 ) % (dyView-1);
				break;
			case KeyEvent.VK_RIGHT:
                movingX = ( movingX + 1 ) % (dxView-1);
				break;
			case KeyEvent.VK_LEFT:
                movingX = ( movingX - 1 + dxView-1 ) % (dxView-1);
				break; 
                /*
			case KeyEvent.VK_Z:
				_myWorld.sd.playSound("audio/click.wav");
                _myWorld.tempCtrl(true, 1);
				break;
                */
			case KeyEvent.VK_X:
                _myWorld.tempCtrl(false, 1);
				break;
			case KeyEvent.VK_C:
                _myWorld.toggleGoogle();
				break;
			case KeyEvent.VK_H:
                //
				break;
			default:
				break;
			}
		}


		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}


		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
}
