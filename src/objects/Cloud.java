// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12

package objects;

import javax.media.opengl.GL2;

import worlds.World;

public class Cloud extends UniqueObject{

	private float high_ratio;
    private float x_ratio;
    private float y_ratio;

	public Cloud (int __x , int __y ,float  high_ratio , World __world,float x_ratio, float y_ratio ) {
		super(__x,__y,__world);
        this.high_ratio = high_ratio;
        this.x_ratio = x_ratio;
        this.y_ratio = y_ratio;
	}


    public void move(){
        int w = world.getWidth();
        int h = world.getHeight();
        int coef = (int)(Math.random()*2);
        switch(World.getWind()){
            case 0 : this.x = (int) (x+coef+w)%w;
                     this.y = (int) (y+coef-h)%h;
                     break;
            case 1 : this.x = (int) (x-coef+w)%w;
                     this.y = (int) (y+coef-h)%h;
                     break;
            case 2 : this.x = (int) (x-coef+w)%w;
                     this.y = (int) (y-coef-h)%h;
                     break;
            case 3 : this.x = (int) (x+coef+w)%w;
                     this.y = (int) (y-coef-h)%h;
                     break;
        }
    }

    public void step(){ //adapted with hight and wind ?
        if (Math.random() < .01 )
            this.move();
    } 

    public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight)
    {
        lenX *= x_ratio;
        lenY *= y_ratio;
        normalizeHeight *= high_ratio;

    	int x2 = (x-(offsetCA_x%myWorld.getWidth()));
    	if ( x2 < 0) x2+=myWorld.getWidth();
    	int y2 = (y-(offsetCA_y%myWorld.getHeight()));
    	if ( y2 < 0) y2+=myWorld.getHeight();
        //rgb(0.00,0.46,0.37)
        if (! world.isRainyWeather()) {
            gl.glColor3f(1f,1f,1f);
        } else 
            gl.glColor3f(0f,0f,0f);
        // The Cloud
        //if (world.isRainyWeather()) gl.glColor3f(0.73f,0.77f,0.80f);//bluergb(0.73,0.77,0.80)
        //else gl.glColor3f(0.79f,1f,0.9f);//WHITE
   
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);//A
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);//B
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);//C
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);//D
        //blanc en face de vert 
        //if (world.isRainyWeather()) gl.glColor3f(0.67f,0.71f,0.75f); //bluergb(0.67,0.71,0.75)
        //else gl.glColor3f(1f,1f,1f);//WHITE
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);

        //if (world.isRainyWeather()) gl.glColor3f(0.60f,0.65f,0.70f); //blue
        //else gl.glColor3f(.94f,0.97f,0.80f);//WHITE
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);

        //if (world.isRainyWeather()) gl.glColor3f(0.53f,0.59f,0.65f); //bluergb(0.53,0.59,0.65)
        //else gl.glColor3f(0.94f,0.97f,1f);//WHITE
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.95f);

        //if (world.isRainyWeather()) gl.glColor3f(0.47f,0.53f,0.60f);//blue
        //else gl.glColor3f(1f,1f,1f);//WHITE
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        
    }
}
