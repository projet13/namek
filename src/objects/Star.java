package objects;

import javax.media.opengl.GL2;
import worlds.World;

public class Star extends UniqueObject{
	private float high_ratio;
    private float x_ratio;
    private float y_ratio;

	public Star ( int __x , int __y ,float  high_ratio , World __world,float x_ratio, float y_ratio )
	{
		super(__x,__y,__world);
        this.high_ratio = high_ratio;
        this.x_ratio = x_ratio;
        this.y_ratio = y_ratio;
	}
	
    public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight)
    {
        //modifications of X,Y,Z must be done in the call in WoldOfTrees
        lenX *= x_ratio;
        lenY *= y_ratio;
        normalizeHeight *= high_ratio;

    	int x2 = (x-(offsetCA_x%myWorld.getWidth()));
    	if ( x2 < 0) x2+=myWorld.getWidth();
    	int y2 = (y-(offsetCA_y%myWorld.getHeight()));
    	if ( y2 < 0) y2+=myWorld.getHeight();


        //diplay only at night
        if (World.morning())
            return;

        // scintillement different, romantic no ? 
        if (Math.random() < .03 ) {
            gl.glColor3f(0f,0f,0f); //black
        } else {
            gl.glColor3f(1f,1f,1.0f);//white
        }
        

        // Model cristal down  comme les coulds  
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);//A
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);//B
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);//C
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);//D

        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);
        
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);

    }
    public void step(){//laisser vide
    	}
    
}
