// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12

package objects;

import javax.media.opengl.GL2;

import worlds.World;

public class Palm extends CommonObject {
    private static float hMax = 10, h = 0;

    public static void displayObjectAt(World myWorld, GL2 gl, int cellState, float x, float y, double height, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight )
    {
                float altitude = (float)height * normalizeHeight ;

                // Vegetation grow periodicly, under sun light, and does not exceed the hMax
                if ((int)System.currentTimeMillis()/1000 % 5 == 0 && h < hMax && World.morning())
                    if (myWorld.isRainyWeather()) h+=0.00002f; //RainyWeather increase the plants growth
                    else  h+=0.00001f;
                
        
                //h= 0.1f;
                //TOP GREEN *(1+h)
                
                switch ( cellState ){
                    case 12:gl.glColor3f(.58f,0.76f,0.02f);break; // GREEN 0 (darker);  // Normal Palm 
                    case 122:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 123:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-0.5f*lenX, offset+y*stepY-0.5f*lenY, altitude*(1+h) + 10.0f);//a(-1,-1)
                gl.glVertex3f( offset+x*stepX-2*lenX, offset+y*stepY+2*lenY, altitude*(1+h) + 10.0f);//b(-1,+1)
                gl.glVertex3f( offset+x*stepX+0.5f*lenX, offset+y*stepY+0.5f*lenY, altitude*(1+h) + 10.0f);//c(+1,+1)
                gl.glVertex3f( offset+x*stepX+2*lenX, offset+y*stepY-2*lenY, altitude*(1+h) + 10.0f);//d(+1,-1)
                
                gl.glVertex3f( offset+x*stepX-2*lenX, offset+y*stepY-2*lenY, altitude*(1+h) + 10.0f);
                gl.glVertex3f( offset+x*stepX-0.5f*lenX, offset+y*stepY+0.5f*lenY, altitude*(1+h) + 10.0f);
                gl.glVertex3f( offset+x*stepX+2*lenX, offset+y*stepY+2*lenY, altitude*(1+h) + 10.0f);
                gl.glVertex3f( offset+x*stepX+0.5f*lenX, offset+y*stepY-0.5f*lenY, altitude*(1+h) + 10.0f);
            
            
                float a = 0.3f; // la base down
                float b = 0.1f; // le haut de l'arbre up
                 
                
                switch ( cellState )
                {
                    case 12:gl.glColor3f(0.77f,0.42f,0.21f); ;break; //BROWN 0 (darker);  // Normal Palm 
                    case 122:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 123:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY-lenY*a, altitude);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY-lenY*a, altitude);

                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY-lenY*a, altitude);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY+lenY*a, altitude);
                
                switch ( cellState )
                {
                    case 12:gl.glColor3f(0.84f,0.48f,0.29f); ;break;//BROWN 1 (light) Normal Palm 
                    case 122:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 123:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY+lenY*a, altitude); // a terre (pas d'altitude)
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY+lenY*a, altitude);

                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY+lenY*a, altitude);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 10.0f);
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY-lenY*a, altitude);

        
    }

}
