package objects;

import javax.media.opengl.GL2;
import worlds.World;

public class ShootingStar extends UniqueObject{
	private float high_ratio;
    private float x_ratio;
    private float y_ratio;
    private boolean display = false;
    private int dir = 1,c=0;
    private int xa,xb,ya,ybb;
    private final int Pshooting = 300; // Probability of a shootong-star 
    // Pshooting increases => fewer shootong-star
    private final int duree = 40;

	public ShootingStar ( int __x , int __y ,float  high_ratio , World __world,float x_ratio, float y_ratio )
	{
		super(__x,__y,__world);
        this.high_ratio = high_ratio;
        this.x_ratio = x_ratio;
        this.y_ratio = y_ratio;
	}
	
    public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight)
    {
        
        if (c % Pshooting == 0) {
            display = ! display;
            c=0;
        }

        if (World.morning() || !display || c % Pshooting > duree)
            return;
    

        //modifications of X,Y,Z must be done in the call in WoldOfTrees
        lenX *= x_ratio;
        lenY *= y_ratio;
        normalizeHeight *= high_ratio;

    	int x2 = (x-(offsetCA_x%myWorld.getWidth()));
    	if ( x2 < 0) x2+=myWorld.getWidth();
    	int yb = (y-(offsetCA_y%myWorld.getHeight()));
    	if ( yb < 0) yb+=myWorld.getHeight();

        // to change the orientation of the ShootingStar
        // magnifique
        switch (dir){
            case 1 :
                xa=x-1;
                ya=y;
                xb=x-2;
                ybb=y;
                break;
            case 2 :
                xa=x-1;
                ya=y-1;
                xb=x-2;
                ybb=y-2;
                break;
            case 3 :
                xa=x;
                ya=y-1;
                xb=x;
                ybb=y-2;
                break;
            case 4 :
                xa=x+1;
                ya=y-1;
                xb=x+2;
                ybb=y-2;//
                break;
            case 5 :
                xa=x+1;
                ya=y;
                xb=x+2;
                ybb=y;
                break;
            case 6 :
                xa=x+1;
                ya=y+1;
                xb=x+2;
                ybb=y+2;
                break;
            case 7 :
                xa=x;
                ya=y+1;
                xb=x;
                ybb=y+2;
                break;
            case 8 :
                xa=x-1;
                ya=y+1;
                xb=x-2;
                ybb=y+2;//
                break;
        }
        gl.glColor3f(0.8f,0.48f,0.81f);// violet
        //gl.glColor3f(1f,1f,1.0f);//white
        // Model cristal down  comme les coulds  
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);//A
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);//B
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);//C
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);//D

        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);

        /*************************************/
        //the tail of the ShootingStar
        x2=xa;
        yb=ya;
        //gl.glColor3f(1f,0.96f,0f);// Yellow
        //gl.glColor3f(0.8f,0.48f,0.81f);// violet
        gl.glColor3f(1f,1f,1f);// violet
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);//A
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);//B
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);//C
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);//D

        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);

        /*********************************/
        // the tail also 
        gl.glColor3f(0.8f,0.48f,0.81f);// violet
        x2=xb;
        yb=ybb;
        //gl.glColor3f(1f,0.96f,0f);// Yellow
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);//A
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);//B
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);//C
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);//D

        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX, offset+yb*stepY, 0.1f*normalizeHeight*0.99f);

        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX-lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY+lenY, 0.1f*normalizeHeight);
        gl.glVertex3f( offset+x2*stepX+lenX, offset+yb*stepY-lenY, 0.1f*normalizeHeight);

    }

    public void step(){ 
        int w = world.getWidth();
        int h = world.getHeight();

        /* Moore mouvements 
                432
                501
                678 
        */   
        

        if (c % Pshooting == 0) {
            dir = (int) (Math.random()*10) % 8 +1;
        }

        int velocity = 2;
        switch (dir){
            case 1 :
                this.x = (int) (x+velocity+w)%w;
                this.y = (int) (y+0+h)%h;
                break;
            case 2 :
                this.x = (int) (x+velocity+w)%w;
                this.y = (int) (y+velocity+h)%h;
                break;
            case 3 :
                this.x = (int) (x+0+w)%w;
                this.y = (int) (y+velocity+h)%h;
                break;
            case 4 :
                this.x = (int) (x-velocity+w)%w;
                this.y = (int) (y+velocity+h)%h;
                break;
            case 5 :
                this.x = (int) (x-velocity+w)%w;
                this.y = (int) (y-0+h)%h;
                break;
            case 6 :
                this.x = (int) (x-velocity+w)%w;
                this.y = (int) (y-velocity+h)%h;
                break;
            case 7 :
                this.x = (int) (x-0+w)%w;
                this.y = (int) (y-velocity+h)%h;
                break;
            case 8 :
                this.x = (int) (x+velocity+w)%w;
                this.y = (int) (y-velocity+h)%h;
                break;
        }
        c++;
        
    	}
    
}
