// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12

package objects;

import javax.media.opengl.GL2;

import worlds.World;

public class Comet extends UniqueObject{
    private float high_ratio;
    private float x_ratio;
    private float y_ratio;

    public Comet ( int __x , int __y ,float  high_ratio , World __world,float x_ratio, float y_ratio )
    {
        super(__x,__y,__world);
        this.high_ratio = high_ratio;
        this.x_ratio = x_ratio;
        this.y_ratio = y_ratio;
    }
    
    public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight)
    {
        float time = System.currentTimeMillis()/1000 % World.getDayLength();
        //comet apear pendant 3 secons only once a day (rule to be changed)
        if (10 >= time || time >= 13 )
            return;

        lenX *= x_ratio;
        lenY *= y_ratio;
        normalizeHeight *= high_ratio;
        /*
        int[][] orang = new int[13][13] = {
            {0,1},{0,2},{0,3},{1,0},{2,0},{2,4},
            {}
        }
        for (int i; i<croix[0].lenght ;i++ ) {
            for (int j; i<croix[1].lenght ;j++ ) {
                
            }
        }

        */
        // orang = 0, bown = 1, red = 2;
        //int[][] croix = {{x,y},{x-1,y-1},{x+1,y+1},{x-1,y+1},{x+1,y-1}};
        int[][]com = {{x,y,0},{x,y+1,0},
         {x,y+2,0},{x,y+3,0}
        ,{x+1,y,0}
        ,{x+2,y+0,0},{x+2,y+4,0}
        ,{x+3,y+4,0},{x+3,y+5,0}
        ,{x+4,y+1,0},{x+4,y+2,0},{x+4,y+3,0},{x+4,y+4,0},{x+4,y+5,0},{x+4,y+6,0}
        ,{x+5,y+2,0},{x+5,y+3,0},{x+5,y+4,0},{x+5,y+5,0},{x+5,y+6,0}
        ,{x+6,y+5,0},{x+6,y+6,0}
        ,{x+7,y+6,0}
        ,{x+9,y+10,0}
        ,{x+13,y+8,0}
        ,{x+1,y+1,1},{x+1,y+2,1},{x+1,y+3,1}///
        ,{x+2,y+1,1},{x+2,y+2,1},{x+2,y+3,1}
        ,{x+3,y+1,1},{x+3,y+2,1},{x+3,y+3,1} 
        ,{x,y+4,2} 
        ,{x+1,y+4,2},{x+1,y+5,2}
        ,{x+2,y+5,2},{x+2,y+6,2}
        ,{x+3,y+0,2},{x+3,y+6,2}
        ,{x+4,y+7,2}
        ,{x+5,y+7,2}
        ,{x+6,y+3,2},{x+6,y+4,2},{x+6,y+7,2}
        ,{x+7,y+5,2},{x+7,y+7,2},{x+7,y+8,2}
        ,{x+8,y+6,2},{x+8,y+8,2}
        ,{x+9,y+7,2}
        ,{x+10,y+7,2}
        ,{x+12,y+9,2}};
        


        for( int[] c : com) {

            int x2 = (c[0]-(offsetCA_x%myWorld.getWidth()));
            if ( x2 < 0) x2+=myWorld.getWidth();
            int y2 = (c[1]-(offsetCA_y%myWorld.getHeight()));
            if ( y2 < 0) y2+=myWorld.getHeight();

            
            switch(c[2]) {
                case 0 : gl.glColor3f(1.0f,0.35f,0.19f); break; //orange
                case 1 : gl.glColor3f(0.58f,0.38f,0.18f); break; //brown 
                case 2 : gl.glColor3f(1.0f,0.18f,0.18f); break; //red 
            }
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);//A
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);//B
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);//C
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);//D
            // cote 
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);
            
            // cote 
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);

            // cote 
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, 0.1f*normalizeHeight);

            // UP
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, 0.1f*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, 0.1f*normalizeHeight);
        }
    }

  //laisser vide
    public void step(){
        int w = world.getWidth();
        int h = world.getHeight();
        this.x = (int) (x-1+w)%w;
        this.y = (int) (y-1+h)%h;
    /*
        boolean moving =false;
        int oldX , oldY;
        if ((int)System.currentTimeMillis()/1000 % World.getDay()*1 == 0) {
            oldX =this.x ; oldY=this.y;
            moving = true;
        } 
        // Stupide rule assuming that the commit take the same diagonal
        // it stop when comback to start point 
        // rule to be changed after 
        if (moving) {
            int w = world.getWidth();
            int h = world.getHeight();
            this.x = (int) (x-1+w)%w;
            this.y = (int) (y-1+h)%h;
            if (this.x == oldX && this.y == oldY) {
                moving = false;
            }
        }*/
    }

}
    
