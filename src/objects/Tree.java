// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12

package objects;

import javax.media.opengl.GL2;

import worlds.World;

public class Tree extends CommonObject {
    
    private static float hMax = 1, h = 0;
    public static void displayObjectAt(World myWorld, GL2 gl, int cellState, float x, float y, double height, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight )
    {  
    		    float altitude = (float)height * normalizeHeight ;
                float a,b;

                a = 1f;
                b = 1f;


                // Vegetation grow periodicly, under sun light, and does not exceed the hMax
                if ((int)System.currentTimeMillis()/1000 % 9 == 0 && h < hMax && World.morning())
                    if (myWorld.isRainyWeather()) h+=0.00002f; //RainyWeather increase the plants growth
                    else  h+=0.00001f;
                
                //TOP GREEN up
                
                switch ( cellState ){
                    case 11:gl.glColor3f(.56f,0.78f,0.18f);break; // GREEN (ligth)
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-lenX, offset+y*stepY-lenY, altitude*(1+h) + 7.5f);
                gl.glVertex3f( offset+x*stepX-lenX, offset+y*stepY+lenY, altitude*(1+h) + 7.5f);
                gl.glVertex3f( offset+x*stepX+lenX, offset+y*stepY+lenY, altitude*(1+h) + 7.5f);
                gl.glVertex3f( offset+x*stepX+lenX, offset+y*stepY-lenY, altitude*(1+h) + 7.5f);

                //contour vert
                switch ( cellState ){
                    case 11:gl.glColor3f(.31f,0.45f,0.09f);break;  // GREEN 0 (darker)
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY-lenY*a, altitude*(1+h)+ 5f);//0.f
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY-lenY*a, altitude*(1+h)+ 5f);
                
                switch ( cellState ){
                    case 11:gl.glColor3f(.38f,0.55f,0.11f);break; // GREEN 1
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY+lenY*a, altitude*(1+h)+ 5f); // a terre (pas d'altitude)
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY+lenY*a, altitude*(1+h)+ 5f);
                
                switch ( cellState ){
                    case 11:gl.glColor3f(.40f,0.57f,0.13f);break; // GREEN 2 
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }               
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY-lenY*a, altitude*(1+h)+ 5f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY+lenY*a, altitude*(1+h)+ 5f);
                
                switch ( cellState ){
                    case 11:gl.glColor3f(.49f,0.69f,0.15f);break; // GREEN 3
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY+lenY*a, altitude*(1+h)+ 5f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 7.5f);
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY-lenY*a, altitude*(1+h)+ 5f);
            




                //sidDES //brown 
                a = 0.4f; // la base down
                b = 0.2f; // le haut de l'arbre up

                
                switch ( cellState ){
                    case 11:gl.glColor3f(0.38f,0.31f,0.22f);break; //BROWN 0 (darker)
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY-lenY*a, altitude);//0.f
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY-lenY*a, altitude);
                
                switch ( cellState ){
                    case 11:gl.glColor3f(0.50f,0.39f,0.28f);break; //BROWN 1 
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY+lenY*a, altitude); // a terre (pas d'altitude)
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY+lenY*a, altitude);
                
                switch ( cellState ){
                    case 11:gl.glColor3f(0.58f,0.47f,0.32f);break; //BROWN 2
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY-lenY*a, altitude);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX+lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX+lenX*a, offset+y*stepY+lenY*a, altitude);
                
                switch ( cellState ){
                    case 11:gl.glColor3f(0.54f,0.44f,0.40f);break; //BROWN 3
                    case 112:gl.glColor3f(1.f-(float)(0.2*Math.random()),0.f,0.f);break; // Burning
                    case 113:gl.glColor3f(0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()),0.f+(float)(0.2*Math.random()));break; // Ash
                }
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY+lenY*a, altitude);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY+lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX-lenX*b, offset+y*stepY-lenY*b, altitude*(1+h)+ 5.f);
                gl.glVertex3f( offset+x*stepX-lenX*a, offset+y*stepY-lenY*a, altitude);
                         
        }
    }


