// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12
package applications.simpleworld;
import cellularautomata.CellularAutomataInteger;
import mathtool.HaltonSequence;
import worlds.World;

public class ForestCA extends CellularAutomataInteger {
	World world;
	private static final double VEGETATION_DENSITY = 0.24; // 0.24 (trees and plantes)
	private static final double PLANTS_TREES = 0.87; //0.87 // 0.60 => 60% plants and 40% trees  
	private static final double PALMS_DENSITY = 0.3; //0.3 Palms density

	public ForestCA ( World __world, int __dx , int __dy)
	{
		super(__dx,__dy,true ); // buffering must be true.
		
		this.world = __world;
	}
	
	// Forest has one volcano
	public void init(int volcX, int volcY)
	{
		// coast-line height threshold - for sandy beaches
		double beachHeight = 0.05*world.getMaxEverHeight();
		for ( int x = 0 ; x != _dx ; x++ )
    		for ( int y = 0 ; y != _dy ; y++ )
    		{
    			if (  world.getCellHeight(x,y) >= 0 )
    			{
    				if (world.getCellHeight(x, y) <= beachHeight) this.setCellState(x, y, 9);
    				else this.setCellState(x, y, 0); // empty
    			}
    			else
    			{
    				this.setCellState(x, y, -1); // water (ignore)
    			}
    		}
		// Generate VEGETATION
		// Generate quasi-random trees with Halton sequence
		int treeThreshold = (int) (VEGETATION_DENSITY * _dx * _dy);
		int coordX, coordY;
		for (int i = 1; i < treeThreshold; i++) {
			coordX = (int) (HaltonSequence.get(i, 3) * _dx);
			coordY = (int) (HaltonSequence.get(i, 5) * _dy);
			if (world.getCellHeight(coordX, coordY) > beachHeight)	{
				if (world.getCellHeight(coordX, coordY) < 0.35*world.getMaxEverHeight()){
					if (Math.random() < PLANTS_TREES)
						this.setCellState(coordX, coordY, 1); //plante
					else 
						this.setCellState(coordX, coordY, 11); //tree
				}
			} else {
				if (world.getCellHeight(coordX, coordY) >=0  && Math.random() < PALMS_DENSITY)
					this.setCellState(coordX, coordY, 12); //palm
			}
		}	
		
		
		// Create volcano
		this.setCellState(volcX, volcY, 5); // active volcano
		System.out.println(volcX);
		System.out.println(volcY);

    	this.swapBuffer();

	}
	
	boolean isLava(int state) {
		// 4 is sleeping volcano. After that, heat in decreasing order
		return (state == 4 || state == 5 || state == 6 || state == 61 || state == 62 || state == 63 || state == 64);
	}
	
	// does lava flow from (sourceX, sourceY) to (x, y)
	private int lavaAttemptFlowHere(int x, int y, int here, int sourceX, int sourceY) {
		int source = this.getCellState(sourceX, sourceY);
		if (!isLava(source)) return here;
		if (world.getCellHeight(sourceX, sourceY) < world.getCellHeight(x, y)) return here;
		// water + lave = obsidian
		if (here == -1)	return 8;
		
		// lava will flow here
		world.setTemperatureCellValue(x, y, world.getTemperatureCellValue(sourceX, sourceY));
		
		if (here == 5 || here == 4) return here;
		if (source == 5) return 6;

		// cool off chance
		boolean coolOff = (Math.random() < 0.9);
		
		if (here == 6) return here;
		if (source == 6) return coolOff ? 61 : 6;
		
		if (here == 61) return here;
		if (source == 61) return coolOff ? 62 : 61;
		
		if (here == 62) return here;
		if (source == 62) return coolOff ? 63 : 62;
		
		if (here == 63) return here;
		if (source == 63) return coolOff ? 64 : 63;
		
		if (here == 64) return here;
		if (source == 64) return coolOff ? 7 : 64;
		
		return 7;
	}


	public void step(TemperatureCA temperatureCA)
	{
		float  time = World.getDayTime();
    	for ( int i = 0 ; i != _dx ; i++ )
    		for ( int j = 0 ; j != _dy ; j++ )
    		{
				int oldState = this.getCellState(i, j);
				int newState = oldState;

				double FIRE =  Math.pow(10, temperatureCA.getCellState(i, j) / 20.0 - 5); //0.001 was Math.pow(10, temperatureCA.getCellState(i, j) / 20.0 - 5

    			switch (oldState) {
				/* BEACH:
					beaches contains some golden "dry-sand "
					"water" tiles next to beach may turn into "wet-sand" tiles due to low tides (La marée)
				*/
    			case -1: // water
    				if (this.getCellState( (i+_dx-1)%(_dx) , j ) == 9||
    					this.getCellState( (i+_dx+1)%(_dx) , j ) == 9||
    					this.getCellState( i , (j+_dy+1)%(_dy) ) == 9||
    					this.getCellState( i , (j+_dy-1)%(_dy) ) == 9) {
	    				if ( 0.2*World.getDayLength() < time && time < 0.5*World.getDayLength() && Math.random() <.3) {
	    					 newState = 91; // become wet sand (the tide)
	    					break;
	    				}
    				}
    				newState = -1;
    				break;
				case 91: // wet-sand
    				if ( time > 0.5*World.getDayLength() && Math.random() <.3) newState = -1;
    				else newState = 91;
					break;
    			case 9: // dry-sand, Spontaneous Palm growth
    				if (Math.random() < 0.0001) newState = 12;
    					break;

				/* Vegetation: 
					0 = empty
					1 = plante (can be eaten by prey)
					11 = tree (existed)
					12 = palm (on beaches)
					...
					1n = ...
				*/
    			case 0: // empty
    				// Spontaneous plante or tree growth
	    			if (world.getCellHeight(i, j) < 0.35*world.getMaxEverHeight()) {
	    				if (Math.random() < 0.0001) newState = 1;
	    				else if (Math.random() < 0.0001) newState = 2;
	    			}
	    			break;
    			case 1: // plante (mangeable)
    				// check if neighbors are burning
    				if ( 
    						this.getCellState( (i+_dx-1)%(_dx) , j ) == 2 || this.getCellState( (i+_dx-1)%(_dx) , j ) == 112 || this.getCellState( (i+_dx-1)%(_dx) , j ) == 122 ||
    						this.getCellState( (i+_dx+1)%(_dx) , j ) == 2 || this.getCellState( (i+_dx+1)%(_dx) , j ) == 112 || this.getCellState( (i+_dx+1)%(_dx) , j ) == 122 ||
    						this.getCellState( i , (j+_dy+1)%(_dy) ) == 2 || this.getCellState( i , (j+_dy+1)%(_dy) ) == 112 || this.getCellState( i , (j+_dy+1)%(_dy) ) == 122 ||
    						this.getCellState( i , (j+_dy-1)%(_dy) ) == 2 || this.getCellState( i , (j+_dy-1)%(_dy) ) == 112 || this.getCellState( i , (j+_dy-1)%(_dy) ) == 122
    					)
    					newState = 2; 
					/*
					 * Probability scale exponentially
					 * (0, 100) degree Celsius corresponds to (e-5,1) chance
    				 */
    				else
    					// spontaneously take fire based on this tile's temperature and rainyWeather
    					if ( Math.random() < FIRE  && !world.isRainyWeather() ) 
    						newState = 2;
    					else
    						// a plante can develop to be a tree 
    						if (Math.random() < 0.0001) newState = 11;
    				break;
    			case 2: // burning plante
    				newState = 3;
    				break;
    			// plante ash
    			case 3: 
    				// wind will scatter ash after 2 iteration
    				if (World.getDayTime() % 2 == 0) newState = 33;
	        		break;
    			case 33:
	        		// ash increases plant growth chance
	        		if (Math.random() < 0.3) newState = 1;
	        		else if (Math.random() < 0.1) newState = 11; //tree
	        		else newState = 0;
	        		break;

    			case 11 : //tree
    				// check if neighbors are burning
    				if ( 
    						this.getCellState( (i+_dx-1)%(_dx) , j ) == 2 || this.getCellState( (i+_dx-1)%(_dx) , j ) == 112 || this.getCellState( (i+_dx-1)%(_dx) , j ) == 122 ||
    						this.getCellState( (i+_dx+1)%(_dx) , j ) == 2 || this.getCellState( (i+_dx+1)%(_dx) , j ) == 112 || this.getCellState( (i+_dx+1)%(_dx) , j ) == 122 ||
    						this.getCellState( i , (j+_dy+1)%(_dy) ) == 2 || this.getCellState( i , (j+_dy+1)%(_dy) ) == 112 || this.getCellState( i , (j+_dy+1)%(_dy) ) == 122 ||
    						this.getCellState( i , (j+_dy-1)%(_dy) ) == 2 || this.getCellState( i , (j+_dy-1)%(_dy) ) == 112 || this.getCellState( i , (j+_dy-1)%(_dy) ) == 122
    					)
    					newState = 112;
					/* 
					 * Not burning
    				 * spontaneously take fire based on this tile's temperature
					 * Probability scale exponentially
					 * (0, 100) degree Celsius corresponds to (e-5,1) chance
    				 */
    				else
    					// 1.1x more resistant to fire than plants
    					if ( 1.1 *Math.random() < FIRE  && !world.isRainyWeather()) 
    						newState = 112;
    				break;
    			case 112: //tree burns 4 s
					if (World.getDayTime() % 4 == 0) newState = 113; 
					break;
    			case 113: //tree ash still 2 s
					if (World.getDayTime() % 2 == 0) newState = 33; 
					break;


    			case 12 : //Palm
    				// check if neighbors are burning
    				if ( 
    						this.getCellState( (i+_dx-1)%(_dx) , j ) == 2 || this.getCellState( (i+_dx-1)%(_dx) , j ) == 112 || this.getCellState( (i+_dx-1)%(_dx) , j ) == 122 ||
    						this.getCellState( (i+_dx+1)%(_dx) , j ) == 2 || this.getCellState( (i+_dx+1)%(_dx) , j ) == 112 || this.getCellState( (i+_dx+1)%(_dx) , j ) == 122 ||
    						this.getCellState( i , (j+_dy+1)%(_dy) ) == 2 || this.getCellState( i , (j+_dy+1)%(_dy) ) == 112 || this.getCellState( i , (j+_dy+1)%(_dy) ) == 122 ||
    						this.getCellState( i , (j+_dy-1)%(_dy) ) == 2 || this.getCellState( i , (j+_dy-1)%(_dy) ) == 112 || this.getCellState( i , (j+_dy-1)%(_dy) ) == 122
    					)
    					newState = 122;
					/* 
					 * Not burning
    				 * spontaneously take fire based on this tile's temperature
					 * Probability scale exponentially
					 * (0, 100) degree Celsius corresponds to (e-5,1) chance
    				 */
    				else
    					// 1.3x more resistant to fire than plants
    					if ( 1.3 *Math.random() < FIRE  && !world.isRainyWeather()) 
    					newState = 122;
    					
    				break; 
    			case 122: // Palm burns 4 s
    					if (World.getDayTime() % 4 == 0) newState = 123;
    					break;
    			case 123: //ash
    					if (World.getDayTime() % 2 == 0) newState = 124; 
						break;
				case 124:
						if (Math.random() < 0.01) newState = 12; //palm
		        		else newState = 9;
		        		break;
    			
	        	// volcano
    			case 4:
    				if (Math.random() < 0.01) newState = 5; // erupts
    				else newState = 4; // copied unchanged
    				break;
    			// active volcano ( 2 iterations ) 
    			case 5:
    				// small chance of stopping
    				if (Math.random() < 0.1) newState = 4;
    				else newState = 5;
    				break;
    			// lava
    			case 6:
    				newState = 61;
    				break;
    			case 61:
    				newState = 62;
    				break;
    			case 62:
    				newState = 63;
    				break;
    			case 63:
    				newState = 64;
    				break;
    			case 64:
    				newState = 7;
    				break;
    			// cooled off lava
    			case 7:
    				// may disappear (scattered) if gets really cool
    				if (Math.random() < 2 - 2 * temperatureCA.getCellState(i, j) / 100) {
    					if (world.getCellHeight(i, j) <= 0) newState = 8; // underwater -> obsidian
    					else newState = 0;	// grass grows
    				}
    				else newState = 7;
    				break;
    			// obsidian (lava x water)
    			case 8:
    				newState = 8;
    				break;
    			}
    			
				// does lava flow here
				
				int tmp = newState;
				// Moore voisinage
    			tmp = lavaAttemptFlowHere(i, j, tmp, (i+1)%_dx, j);
    			tmp = lavaAttemptFlowHere(i, j, tmp, i, (j+1)%_dy);
    			tmp = lavaAttemptFlowHere(i, j, tmp, (i-1+_dx)%_dx, j);
    			tmp = lavaAttemptFlowHere(i, j, tmp, i, (j-1+_dy)%_dy);
    			tmp = lavaAttemptFlowHere(i, j, tmp, (i-1+_dx)%_dx, (j-1+_dy)%_dy);
    			tmp = lavaAttemptFlowHere(i, j, tmp, (i+1+_dx)%_dx, (j-1+_dy)%_dy);
    			tmp = lavaAttemptFlowHere(i, j, tmp, (i-1+_dx)%_dx, (j+1+_dy)%_dy);
    			tmp = lavaAttemptFlowHere(i, j, tmp, (i+1+_dx)%_dx, (j+1+_dy)%_dy);
    			if (tmp != newState) newState = tmp;
    			
    			// commit update
    			this.setCellState(i, j, newState);
    			
    			// update color
	    		float color[] = new float[3];
	    		switch ( this.getCellState(i, j) )
	    		{
	    			// dry sand
	    			// (194, 178, 128)
	    			case 9 : 
	    				color[0] = 0.76f; color[1] = 0.69f; color[2] = 0.50f; 
	    				break;
	    			// wet sand
	    			// (174,141,112)
	    			case 91 :
	    				color[0] = 0.68f; color[1] = 0.55f; color[2] = 0.44f; 
	    				break;
	    			case 3: case 123: case 113:  
	    				color[0] = 0.33f;
	    				color[1] = 0.33f;
	    				color[2] = 0.33f;
	    				break;
	    			// lava, volcano tiles
	    			// (217,81,49)
	    			case 4:
	    				color[0] = 0.85f;
	    				color[1] = 0.32f;
	    				color[2] = 0.19f;
	    				break;
	    			// (255,92,0)
	    			case 5:
	    				color[0] = 1.f;
	    				color[1] = 0.36f;
	    				color[2] = 0.f;
	    				break;
	    			// (217,81,49)
	    			case 62:
	    				color[0] = 0.85f;
	    				color[1] = 0.32f;
	    				color[2] = 0.19f;
	    				break;
	    			// (250,103,52)
	    			case 6:
	    				color[0] = 0.98f;
	    				color[1] = 0.40f;
	    				color[2] = 0.20f;
	    				break;
	    			// (250,103,52)
	    			case 61:
	    				color[0] = 0.98f;
	    				color[1] = 0.40f;
	    				color[2] = 0.20f;
	    				break;
	    			// (108,108,108)
	    			case 63:
	    				color[0] = 0.39f;
	    				color[1] = 0.39f;
	    				color[2] = 0.39f;
	    				break;
	    			// (108,108,108)
	    			case 64:
	    				color[0] = 0.39f;
	    				color[1] = 0.39f;
	    				color[2] = 0.39f;
	    				break;
	    			//  (69,69,69)
	    			case 7:
	    				color[0] = 0.27f;
	    				color[1] = 0.27f;
	    				color[2] = 0.27f;
	    				break;
	    			//   (126,29,251)
	    			case 8:
	    				color[0] = 0.49f;
	    				color[1] = 0.11f;
	    				color[2] = 0.98f;
	    				break;	
	    			default:
	    				color = world.coloring((float)world.getCellHeight(i, j), (float)world.getTemperatureCellValue(i, j));break;
	    		}

	    		// Changement de couleur avec le temps 
		        if (time <= World.getSunrise() + World.getAnimationTime()) {//sunrise 
		        	color[0] = (float) (10*(color[0]*(1-0.299))*time/World.getDayLength() + 0.299*color[0]);
		            color[1] = (float) (10*(color[1]*(1-0.587))*time/World.getDayLength() + 0.587*color[1]);
		            color[2] = (float) (10*(color[2]*(1-0.114))*time/World.getDayLength() +0.114*color[2]);
		        } else if ( time <= World.getSunset() ) {
		        	//world.DAY time, natural colors
		        } else if ( time <= World.getSunset() + World.getAnimationTime() ) {
		        	//System.out.println("SUNSIT"+time);
		        	color[0] = (float) (-10*(color[0]*(1-0.299))*(time -World.getSunset() -World.getAnimationTime())/World.getDayLength() + 0.299*color[0]);
		            color[1] = (float) (-10*(color[1]*(1-0.587))*(time-World.getSunset() -World.getAnimationTime())/World.getDayLength() + 0.587*color[1]);
		            color[2] = (float) (-10*(color[2]*(1-0.114))*(time-World.getSunset() -World.getAnimationTime())/World.getDayLength() + 0.114*color[2]);
		        } else {
		        	// night time
		        	color[0] *= 0.299;
		            color[1] *= 0.587;
		            color[2] *= 0.114;
		        }
	        

	    		this.world.cellsColorValues.setCellState(i, j, color);
    		}
    	this.swapBuffer();
	}

	
}
