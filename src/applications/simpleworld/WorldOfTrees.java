// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12

package applications.simpleworld;

import javax.media.opengl.GL2;

import Agent.Fish;
import Agent.Predator;
import Agent.Prey;
import objects.*;
import worlds.World;
import mathtool.HaltonSequence;

public class WorldOfTrees extends World {
	protected ForestCA forestCA;
    protected TemperatureCA temperatureCA;  
    private final double PREDATOR_RATIO = 0.3;
    private final int MAX_LAND_POPULATION = 80;
    private final int MAX_SEA_POPULATION = 10;
    private int landPopulation;
    private int seaPopulation;
    
	// coordinates of ONE volcano
    int volcX;
	int volcY;

    public void init ( int __dxCA, int __dyCA, double[][] landscape ){
    	
    	super.init(__dxCA, __dyCA, landscape);      
    	
    	// add colors
    	for ( int x = 0 ; x < __dxCA ; x++ )
    		for ( int y = 0 ; y < __dyCA ; y++ )
    		{
    			float altitude = (float) this.getCellHeight(x, y);
    			float temp = (float) this.getTemperatureCellValue(x, y);
    			float color[] = coloring(altitude, temp);
    			this.cellsColorValues.setCellState(x, y, color);
    		}
    	
    	//Add vegetations (Trees + Plantes)
    	landPopulation = 0;
    	seaPopulation = 0;
    	int halton, coordX, coordY;
    	halton = 0;
    	// keep trying to spawn when one max population not reached yet
    	while (landPopulation < MAX_LAND_POPULATION || seaPopulation < MAX_SEA_POPULATION) {
    		
    		halton++;
    		// I choose base 5 and 7 for Halton Sequence
    		coordX = (int) (dxCA * HaltonSequence.get(halton, 5));
    		coordY = (int) (dxCA * HaltonSequence.get(halton, 7));
    		
    		// If underwater and not reached max population, spawn fish
    		if (this.getCellHeight(coordX, coordY) <= 0) {
    			if (seaPopulation < MAX_SEA_POPULATION) {
    				uniqueDynamicObjects.add(new Fish(coordX, coordY, this));
    				seaPopulation++;
    			}
				continue;
    		};
    		
    		// If the code reaches here, it means max land population not reached and is above water. Spawn animal
    		if (Math.random() < PREDATOR_RATIO) {
    			uniqueDynamicObjects.add(new Predator(coordX,coordY,this));
    			landPopulation++;
    		}
    		else {
    			uniqueDynamicObjects.add(new Prey(coordX,coordY,this));
    			landPopulation++;
    		}
    	}

    	/*
    // I don't want to have a bridge
    for ( int i = 0 ; i < 11 ; i++ )
    {
        if ( i%10 == 0 )
            uniqueObjects.add(new Monolith(50,50+i,this));
        else
            uniqueObjects.add(new BridgeBlock(50,50+i,1,this,1,1));
    }
    	 */

    	//add some  stars
    	for ( int i = 0 ; i < 60 ; i++ ){
    		uniqueObjects.add(new Star((int)(500*Math.random()) % __dxCA,(int)(500*Math.random()) % __dyCA,
    				(float)Math.random()*3+6,this,0.1f,.1f)); //7 is the high minimum of a start
    	}

    	// clouds
    	for ( int i = 0 ; i < 8; i++ ){
    		uniqueObjects.add(new Cloud((int)(500*Math.random()) % __dxCA,(int)(300*Math.random()) % __dyCA,
    				(float)Math.random()*5+2,this,(float)Math.random()*4+8,2+(float)Math.random()*2f));  
    	}
    	//uniqueObjects.add(new BridgeBlock(56,56,4,this,1,1));
    	//Add sun after
    	//uniqueObjects.add(new Sun(128,50,7,this,25,25));

    	//add comet
    	uniqueObjects.add(new Comet(dxCA,dyCA,6,this,1,1)); 
    	// ShootingStar Etoile filante *.*
    	uniqueObjects.add(new ShootingStar(50,50,7,this,0.3f,0.3f)); 
    }
	
	public float[] coloring(float altitude, float temp) {
         
        /* La colorisation se fait en fonction de deux parametre
        la temperature et la hauteur 
        */
		float color[] = new float[3];
		if ( temp < -15) { 
			// -15 degree : (177,196,216)
            //height condition momentally added must correct TemperatureCA
                color[0] = 0.69f;
                color[1] = 0.77f;
                color[2] = 0.84f;
            
        	
		}
		else if ( temp < 0) { 
			// -15 degree : (177,196,216) -> 0 degree : (256,256,256)
            //height condition momentally added must correct TemperatureCA
                color[0] = 0.69f + 0.31f * (temp + 15) / 15;
                color[1] = 0.77f + 0.23f * (temp + 15) / 15;
                color[2] = 0.84f + 0.16f * (temp + 15) / 15;
            
        	
		}
		else {
			float height = altitude / (float) this.getMaxEverHeight();
	        if ( height >= 0 )
	        {
				if (height < 0.2) {
					// (71, 218, 116) -> (30, 196, 100)
                    
					color[0] = 0.28f - 0.16f * height / 0.2f;
					color[1] = 0.85f - 0.08f * height / 0.2f;
					color[2] = 0.45f - 0.06f * height / 0.2f;
                    /*
                    color[0] = 0.33f - 0.1f * (height - 0.6f) / 0.2f;
                    color[1] = 0.25f - 0.11f * (height - 0.6f) / 0.2f;
                    color[2] = 0.14f - 0.06f * (height - 0.6f) / 0.2f;
                    */
				}
				else if (height < 0.4) {
					// (30, 196, 100) -> (35, 144, 79)
					color[0] = 0.12f + 0.02f * (height - 0.2f) / 0.2f;
					color[1] = 0.77f - 0.21f * (height - 0.2f) / 0.2f;
					color[2] = 0.39f - 0.08f * (height - 0.2f) / 0.2f;
				}
				else if (height < 0.6) {
					// (35, 144, 79) -> (85, 65, 36)
					color[0] = 0.14f + 0.19f * (height - 0.4f) / 0.2f;
					color[1] = 0.56f - 0.31f * (height - 0.4f) / 0.2f;
					color[2] = 0.31f - 0.17f * (height - 0.4f) / 0.2f;
				}
				else if (height < 0.8) {
					// (85, 65, 36)
					color[0] = 0.33f - 0.1f * (height - 0.6f) / 0.2f;
					color[1] = 0.25f - 0.11f * (height - 0.6f) / 0.2f;
					color[2] = 0.14f - 0.06f * (height - 0.6f) / 0.2f;
				}
				else {
					// (60, 37, 21)
					color[0] = 0.23f;
					color[1] = 0.14f;
					color[2] = 0.08f;
				}
	        }
		    else
		    {
		        // water : blue more darker with depth
				float depth = altitude / ( (float) this.getMinEverHeight() );
				color[0] = (0.15f - 0.15f * depth);
				color[1] = (0.15f - 0.15f * depth);
				color[2] = (1.0f - 0.15f * depth);
		    }
		}
        return color;
	}

    
    
    protected void initCellularAutomata(int __dxCA, int __dyCA, double[][] landscape)
    {
    	boolean dayTime = ((System.currentTimeMillis()/1000) % World.getDayLength() < World.getSunset());
    	temperatureCA = new TemperatureCA(this, dxCA, dyCA, dayTime);
    	temperatureCA.init();

    	forestCA = new ForestCA(this,__dxCA,__dyCA);

    	// find mountain peak (not necessarily max height)
    	volcX = 0;
    	volcY = 0;
    	double max_height = Double.NEGATIVE_INFINITY;
    	for (int i = 0; i < __dxCA; i++) {
    		for (int j = 0; j < __dyCA; j++) {
    			if (this.getCellHeight(i, j) > 0.8 * max_height) {
    				if (this.getCellHeight(i, j) > max_height) max_height = this.getCellHeight(i, j);
    				volcX = i;
    				volcY = j;
    			}
    		}
    	}
    	forestCA.init(volcX, volcY);
    }
    
    protected void stepCellularAutomata()
    {
    	if ( iteration%50 == 0 ) {
       		forestCA.step(temperatureCA);
       		temperatureCA.step(forestCA);
    	}
    }
    

    protected void stepAgents()
    {
    	// nothing to do.
    	for ( int i = 0 ; i < this.uniqueDynamicObjects.size() ; i++ )
    	{
    		this.uniqueDynamicObjects.get(i).step();
    	}
    }
    
    protected void stepObj(){
        for ( int i = 0 ; i < this.uniqueObjects.size() ; i++ )
        {
            this.uniqueObjects.get(i).step();
        }
    }

    public int getForestCellValue(int x, int y) // used by the visualization code to call specific object display.
    {
    	return forestCA.getCellState(x%dxCA,y%dyCA);
    }

    public void setForestCellValue(int x, int y, int state)
    {
    	forestCA.setCellState( x%dxCA, y%dyCA, state);
    }
    
    public double getTemperatureCellValue(int x, int y)
    {
    	return temperatureCA.getCellState(x%dxCA, y%dyCA);
    }
    
    public void setTemperatureCellValue(int x, int y, double t)
    {
    	temperatureCA.setCellState(x%dxCA, y%dyCA, t);
    }
    
    public void tempCtrl(boolean increase, float amount) {
    	if (increase) temperatureCA.inputHeatIncrement+=amount;
    	else temperatureCA.inputHeatIncrement-=amount;
    }
    
    public void toggleGoogle() {
    	temperatureCA.toggleGoogle();
    }
    
	public void displayObjectAt(World _myWorld, GL2 gl, int cellState, int x,
			int y, double height, float offset,
			float stepX, float stepY, float lenX, float lenY,
			float normalizeHeight) { 
		switch ( cellState )//
		{
        case 1: case 2: case 3:
            Plante.displayObjectAt(_myWorld,gl,cellState, x, y, height, offset, stepX, stepY, lenX, lenY, normalizeHeight); break;
		case 11: case 112: case 113:
			Tree.displayObjectAt(_myWorld,gl,cellState, x, y, height, offset, stepX, stepY, lenX, lenY, normalizeHeight); break;
		case 12: case 122: case 123: 
            Palm.displayObjectAt(_myWorld,gl,cellState, x, y, height, offset, stepX, stepY, lenX, lenY, normalizeHeight); break;
        }
	}

	//public void displayObject(World _myWorld, GL2 gl, float offset,float stepX, float stepY, float lenX, float lenY, float heightFactor, double heightBooster) { ... } 
    
   
}
