package applications.simpleworld;

import java.util.ArrayList;
import java.util.Collections;

import cellularautomata.CellularAutomataDouble;
import worlds.World;

public class TemperatureCA extends CellularAutomataDouble {
	World world;	// not needed unless for testing
	public double inputHeatIncrement = 0;
	private boolean heatGoogle;
	private final double heatLossToSpace = -0.01;
	private final double seasonalHeatWeight = 0.6;
	private final double dailyHeatWeight = 0.6;
    private static double temperatureWorld = 0;
    
    
	
	public TemperatureCA(World __world, int dx, int dy, boolean dayTime) {
		super(dx, dy, true);
		inputHeatIncrement = 0;
		this.world = __world;
		heatGoogle = false;
	}
	
	public void init() {
    	// Initialize temperature in river and land
		for ( int x = 0 ; x != _dx ; x++ )
    		for ( int y = 0 ; y != _dy ; y++ ) {
    			if ( world.getCellHeight(x, y) / world.getMaxEverHeight() >= 0.8) {
    				this.setCellState(x, y, -50); // cold snowy mountain
    			}
    			else if (   world.getCellHeight(x, y) >= 0 ) {
    				this.setCellState(x, y, 15); // hot on land
    			}
    			else {
    				this.setCellState(x, y, 5); // cool in water
    			}
    		}
    	this.swapBuffer();
	}
	
	public void toggleGoogle() {
		heatGoogle = !heatGoogle;
	}
	
	public void step(ForestCA forestCA) {
		double seasonalHeatIncrement, dailyHeatIncrement, totalHeatIncrement;
		// heat variation in seasons
		switch (World.getSeason()) {
            case 0: // starting spring
                seasonalHeatIncrement = 1;
                break;
            case 1: // ending spring
                seasonalHeatIncrement = 0;
                break;
            case 2:
                // starting summmer
                seasonalHeatIncrement = 2;
                break;
            case 3:
                // ending summer
                seasonalHeatIncrement = 0;
                break;
            case 4: case 5: // automn
                // starting automn 
                seasonalHeatIncrement = -1;
                break;
            case 6:
                // starting winter
                seasonalHeatIncrement = -2;
                break;
            case 7:
                // ending winter
                seasonalHeatIncrement = 1;
                break;
            default:
                seasonalHeatIncrement = 0;
                break;

		}
		// heat variation during the day.
		// Daytime lasts longer than nighttime, so heat gain per sec during day time must be smaller to keep balance
		if (World.morning()) dailyHeatIncrement = (World.getSunrise() + World.getDayLength() - World.getSunset()) / World.getDayLength();
		else dailyHeatIncrement = (World.getSunset() - World.getSunrise()) / World.getDayLength();
		
		totalHeatIncrement = seasonalHeatWeight * seasonalHeatIncrement + dailyHeatWeight * dailyHeatIncrement + heatLossToSpace;
		/* 
		 * Asynchronous
    	 */
		ArrayList<Integer> permutation = new ArrayList<Integer>();
		for (int i = 0; i < _dx * _dy; i++) {
			permutation.add(i);
		}
		Collections.shuffle(permutation);
		
    	for ( int next : permutation ) {
    			int i = next%_dx;
    			int j = next/_dx;
    			// get temperatures
    			double t  = this.getCellState( i , j );
    			double t1 = this.getCellState( (i+_dx-1)%(_dx) , j );
    			double t2 = this.getCellState( (i+_dx+1)%(_dx) , j );
    			double t3 = this.getCellState( i , (j+_dy+1)%(_dy) );
    			double t4 = this.getCellState( i , (j+_dy-1)%(_dy) );
    			double tAdjacent = (t1 + t2 + t3 + t4)/4;
    			
    			// temporary value for later update
    			double transferRate = 8;
    			t1 += (t - t1)/transferRate;
    			t2 += (t - t2)/transferRate;
    			t3 += (t - t3)/transferRate;
    			t4 += (t - t4)/transferRate;
    			
    			t += (tAdjacent - t) * 4 / transferRate;
    			
    			// colder in higher terrain
    			if ( world.getCellHeight(i, j) / world.getMaxEverHeight() >= 0.8) t -= 6;
    			
    			// artificial adjustment - let's pretend this is sunlight!
    			if (forestCA.getCellState(i, j) != -1 && t < 100 && t > -100) t += totalHeatIncrement * 1.5/* * 5 for madness */;
    			
    			// update me
    			this.setCellState(i, j, t);
    			
    			// update adjacent tiles
    			this.setCellState( (i+_dx-1)%(_dx) , j , t1);
    			this.setCellState( (i+_dx+1)%(_dx) , j , t2);
    			this.setCellState( i , (j+_dy+1)%(_dy) , t3);
    			this.setCellState( i , (j+_dy-1)%(_dy) , t4);
    		}
    	
    	// Burning trees give off heat (+20 degree Celsius)
    	// Sleeping volcano is 300 degree
    	// Erupting volcano is 1500 degree
    	// Flowing lava is 150 degree
    	for (int i = 0;  i != _dx ; i++ )
    		for (int j = 0; j != _dy ; j++ ) {
    			int state = forestCA.getCellState(i, j);
    			if (state == 2) {
    				this.setCellState(i, j, this.getCellState(i, j) + 10);
    			}
    			else if (state == 4) {
    				this.setCellState(i, j, 300);
    			}
    			else if (state == 5) {
    				this.setCellState(i, j, 1500);
    			}
    			else if (forestCA.isLava(state)) {
    				this.setCellState(i, j, 150);
    			}
    			else if (state == -1) {
    				if (this.getCellState(i, j) > 10) this.setCellState(i, j, 10);
    			}
    		}
    	
    	if (heatGoogle) {
        	// Display mode: heat google.
        	for (int i = 0;  i != _dx ; i++ )
        		for (int j = 0; j != _dy ; j++ ) {
        			float color[] = new float[3];
        			float temp = Math.min((((float) this.getCellState(i, j)) / 100), 1);
    				color[0] = temp;
    				color[1] = temp;
    				color[2] = temp;
    				this.world.cellsColorValues.setCellState(i, j, color);
        		}
    	}
        // calculate temperature every 20 iterations
    	if (World.getIteration() % 20 == 0) {
        double s = 0;
        for (int i = 0; i != _dx ; i++ ) {
            for (int j = 0; j != _dy ; j++ ) {
                // ne pas tenir compte volcons et montagnes
                if ( world.getCellHeight(i, j) < 0.8)
                    s +=   getCellState(i, j);
            }
        }
        temperatureWorld = s / (_dx*_dy);
        }

		this.swapBuffer();
	}
	
    public static double getTemperatureWorld(){
        return temperatureWorld;
    }
   
    
}

