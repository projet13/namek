package Agent;
import javax.media.opengl.GL2;
import mathtool.PathFinder;
import objects.UniqueDynamicObject;
import worlds.World;

public class Predator extends Agent {
	
	Prey target;
	
	int patience;	// how determined I am to chasing my target
	
	public Predator ( int __x , int __y, World __world )
	{
		super(__x,__y,__world);
		target = null;
		patience = 100;
	}
	
	private Prey findNearbyPrey() {
		// am i already chasing someone? is my prey stolen? do i still want to chase my target?
		if (target == null || target.isAlive() == false || patience <= 0) {
			// find new target
			patience = 70;
			int min_distance = Integer.MAX_VALUE;
			int distance;
			for (UniqueDynamicObject a : world.getUniqueDynamicObjects()) {
				if (!(a instanceof Prey)) continue;
				Prey prey = (Prey) a;
				if (prey.isAlive() == false) continue;	// Don't eat carcass
				distance = PathFinder.distance(x, y, prey.x(), prey.y(), world.getWidth(), world.getHeight());
				if (distance > 20) continue;	// Eye-sight 20
				if (distance < min_distance) {
					min_distance = distance;
					target = prey;
				}
			}
		}
		return target;
	}
	
	private void hunt(Prey prey) {
		int distance = PathFinder.distance(x, y, prey.x(), prey.y(), world.getWidth(), world.getHeight());
		 // Lunge towards if in range
		if (distance < 2) {
			x = prey.x();
			y = prey.y();
			prey.hp = -1;
			hunger = 0;
			target = null;
		}
		// Lose patience if still not caught
		else {
			patience--;
		}
	}
	
	// return true if succeed
	private boolean tryHunt() {
		Prey prey = findNearbyPrey();
		// Fail to find
		if (prey == null) {
			return false;
		}
		// Which step toward goal?
		direction = PathFinder.approach(x, y, prey.x(), prey.y(), world.getWidth(), world.getHeight());
		// move there
		moveTo(direction);
		if (Math.random() < 0.001*hp) {
			// bonus movement
			moveTo(direction);
		}
		// kill
		hunt(prey);
		return true;
	}
	
	protected void dayTimeStep() {
		// If hungry, try hunt
		if (hunger > 100) {
			boolean hunted = tryHunt();
			if (hunted) return;	// If hunted, no time to drink
		}
		// If thirsty, find water
		if (thirst > 150) {
			tryDrink();
		}
		// If fine, move randomly
		else stepRand();

		// Make predator sound
		if (Math.random() < 0.01) {
			world.sd.playSound("audio/predator.wav");
		}
	}
	
	protected void nightTimeStep() {
		// goes uphill
		int dx = world.getWidth();
		int dy = world.getHeight();
		int x = this.x;
		int y = this.y;
		double rightHeight = world.getCellHeight((x + 5) % dx, y);
		double upHeight = world.getCellHeight(x, (y - 5 + dy) % dx);
		double leftHeight = world.getCellHeight((x - 5 + dx) % dx, y);
		double downHeight = world.getCellHeight(x, (y + 5) % dy);
		
		double maxHeight = world.getCellHeight(x, y);
		direction = 0; // stay
		if (rightHeight > maxHeight) {
			direction = 1; // right
			maxHeight = rightHeight;
		}
		if (upHeight > maxHeight) {
			direction = 2; // up
			maxHeight = upHeight;
		}
		if (leftHeight > maxHeight) {
			direction = 3; // left
			maxHeight = leftHeight;
		}
		if (downHeight > maxHeight) {
			direction = 4; // down
			maxHeight = downHeight;
		}
		
		moveTo(direction);

		// Make sleeping sound
		if (Math.random() < 0.001) {
			world.sd.playSound("audio/sleeping.wav");
		}
	}
	
	protected void spawn() {
		for (UniqueDynamicObject o : world.getUniqueDynamicObjects()) {
			if (!(o instanceof Predator)) continue;
			Predator offspring = (Predator) o;
			if (offspring.isAlive()) continue;
			if (!canGo((x+1)%world.getWidth(), (y+1)%world.getHeight())) continue;
			offspring.hp = 75;
			offspring.x = (x+1)%world.getWidth();
			offspring.y = (y+1)%world.getHeight();
			return;
		}
	}
	
    public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight){
    	// do not display if dead
    	if (isAlive()) {
           	int x2 = (x-(offsetCA_x%myWorld.getWidth()));
        	if ( x2 < 0) x2+=myWorld.getWidth();
        	int y2 = (y-(offsetCA_y%myWorld.getHeight()));
        	if ( y2 < 0) y2+=myWorld.getHeight();

        	float height = Math.max ( 0 , (float)myWorld.getCellHeight(x, y) );

        	float a = 0.2f; // la base down
            float b = 0.2f; // le haut  up
            float z0 = 0;
            float c = 1f;
            float dd = 0.7f;
            
            // legs Brown
             	gl.glColor3f(0.1f,0.1f,0.1f); // brown 
	            // patte 1 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 2 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 3 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 4 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);


            a = 0.4f;
            b = 0.4f;
            z0 += c;
            c =1f ;
	        // legs white
             	gl.glColor3f(0.05f,0.05f,0.05f);
	            // patte 1 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 2 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 3 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 4 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	        a = 1.1f;
            b = 1.1f;
            z0 += c; 
            c =1.8f;
            // Body
             	gl.glColor3f(0.20f,0.20f,0.20f);
	            // Sides
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);  
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // Top
	            gl.glColor3f(0.25f,0.25f,0.25f);
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);

	            
	        a = 0.7f;
            b = 0.7f; float tmp = z0 + c;
            z0 += 0.5f*c; 
            dd = 1f;
            c =2f;
            float ddx = dd*lenX, ddy = -dd*lenY;
            switch (direction) {
            	case 1 :
            		ddy = 0;
            		break;
            	case 2 :
            		ddx = 0;
            		break;
            	case 3 :
            		ddy = 0;
            		ddx *= -1;
            		break;
            	case 4:
            		ddx = 0;
            		ddy *= -1;
            		break;
            	default:
            		ddy = 0;
            		break;
            }

	        // Head
        		// Sides
        		gl.glColor3f(0f,0.0f,0f); 
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); 
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // Top
            	gl.glColor3f(0f,0f,0f); // white
            	if (hunger > 100 && System.currentTimeMillis()/500 % 2 == 0) gl.glColor3f(1.0f,0f,0.0f);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);
	        

	        a = 0.2f;
            b = 0.08f;
            z0 += 0.5f*c;
            c =2.5f;
            //Ears
            gl.glColor3f(0.10f,0.10f,0.10f);
            // ear 1
	            if (direction == 1 || direction == 3 || direction == 0) {
            		ddy -= 0.4f;
	            } else {
	            	ddx -= 0.4f;
	            }
        		//gl.glColor3f(0.92f,0.33f,0.71f); // pink
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); 
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	        // ears 2
	            if (direction == 1 || direction == 3 || direction == 0) {
	            	ddx += 0.0f;
		            ddy += 0.4f;
		            ddy += 0.4f;
	            } else {
	            	ddy += 0.0f;
		            ddx += 0.4f;
		            ddx += 0.4f;
	            } 
	            
        		//gl.glColor3f(0.62f,0.50f,0.79f); // pink
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); 
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            
	        
	        a = 0.2f;
            b = 0.01f;
            z0 = tmp;
            c =2f;
            ddx -= 0.2f;
	        ddy -= 0.2f;
            ddy *= -1;
            ddx *= -1;
	        // tail
        		// Sides
        		gl.glColor3f(0f,0f,0f); 
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); 
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
    	}
    }
}