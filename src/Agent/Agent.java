package Agent;
import javax.media.opengl.GL2;
import mathtool.PathFinder;
import objects.UniqueDynamicObject;
import worlds.World;

public abstract class Agent extends UniqueDynamicObject{
	
	protected int hp; // 0 dead 75 spawn max 125
	protected int hunger; // 0 means ok, 600 means hungry
	protected int thirst; // 0 means ok, 600 means thirsty
	
	/** direction **/
	protected int direction; 
	/* Codes of mvt :
		 	2
			^
		3 <-0-> 1
			v
			4
	 */
	
	public Agent ( int __x , int __y, World __world )
	{
		super(__x,__y,__world);
		hp = 100;
		hunger = 0;
		thirst = 0;
		direction = 0;
	}

	public boolean isAlive() {
		return (hp > 0) ;
	}
	/** Coordinates getters **/
	protected int x() {
		return x;
	}
	protected int y() {
		return y;
	}
	
	/** Mvt functions **/
	// mvt abilility ?
	public boolean canGo(int x, int y) {
		if (world.getCellHeight(x, y) < 0) return false;			//undergroud and water
		if (world.getForestCellValue(x, y) == 2) return false;		// Can't walk on fire
		if (world.getTemperatureCellValue(x, y) > 60) return false;	// Too hot
		for (UniqueDynamicObject a : world.getUniqueDynamicObjects()) { // pas de tamponage by individuals of same species
				Agent ag = (Agent)a;
				if (ag.getClass() == getClass() && ag.x() == x && ag.y() == y) return false;
		}
		return true;
	}
	
	// Moving L-R-U-D (return true if succeed)
	protected boolean goLeft() {
		int w = world.getWidth();
		int left = (x-1+w)%w;
		if (canGo(left, y)) {
			x = left;
			return true;
		}
		else
			return false;
	}

	protected boolean goRight() {
		int w = world.getWidth();
		int right = (x+1)%w;
		if (canGo(right, y)) {
			x = right;
			return true;
		}
		else
			return false;
	}
	
	protected boolean goUp() {
		int h = world.getHeight();
		int up = (y-1+h)%h;
		if (canGo(x, up)) {
			y = up;
			return true;
		}
		else
			return false;
	}
	
	protected boolean goDown() {
		int h = world.getHeight();
		int down = (y+1)%h;
		if (canGo(x, down)){
			y = down;
			return true;
		}		
		else
			return false;
			
	}

	// change direction randomly
	protected void changeDir() {
		direction = (int) (Math.random() * 5);
	}

	
	// Purposefully move to my destination
	protected void moveTo(int dest, int attemptsLeft) {
		if (attemptsLeft <= 0) return;
		boolean succeed;
		switch (dest) {
		case 0:
			return;
		case 1:
			succeed = goRight();
			break;
		case 2:
			succeed = goUp();
			break;
		case 3:
			succeed = goLeft();
			break;
		case 4:
			succeed = goDown();
			break;
		default:
			succeed = false;
		}
		if (!succeed) {
			// If stuck, attempt another direction
			changeDir();
			moveTo(direction, attemptsLeft - 1);
		}
	}
	
	protected void moveTo(int direction) {
		moveTo(direction, 4);	// default 4 attempts
	}
	
	// Random movement
	public void stepRand() {
		// 16% chance to change direction
		// 80% if standing still
		if ( Math.random() < 0.8 && (Math.random() < 0.2 || direction == 0)) {
			changeDir();
		}
		// walk
		moveTo(direction);
	}

	
	/**************** Trying to survive *******************/
	//Searching water :
	// return coordinates of nearby water
	private int[] findNearbyWater() {
		// Find in this proximity
		//			*
		//		*	*	*
		//	*	* agent	*	*
		//		*	*	*
		//			*
		int dx = world.getWidth();
		int dy = world.getHeight();
		int[][] toFind = new int[][] {
			{(x+3)%dx, y}, {x, (y+3)%dy}, {(x-3+dx)%dx, y}, {x, (y-3+dy)%dy},
			{(x+6)%dx, y}, {x, (y+6)%dy}, {(x-6+dx)%dx, y}, {x, (y-6+dy)%dy},
			{(x+3)%dx, (y+3)%dy}, {(x+3)%dx, (y-3+dy)%dy}, {(x-3+dx)%dx, (y-3+dy)%dy},
			{(x-3+dx)%dx, (y+3)%dy}
		};
		for (int[] coord : toFind) {
			if (world.getForestCellValue(coord[0], coord[1]) == -1) //water = -1 
				return coord;
		}
		return null;
	}
	
	// try drinking from adjacent water
	private void drink() {
		int dx = world.getWidth();
		int dy = world.getHeight();
		if (	world.getForestCellValue((x+1)%dx, y) == -1		||
				world.getForestCellValue((x-1+dx)%dx, y) == -1	||
				world.getForestCellValue(x, (y+1)%dy) == -1		||
				world.getForestCellValue(x, (y-1+dy)%dy) == -1) {
			thirst = 0;
		}
	}
	
	// I'm thirsty!
	protected void tryDrink() {
		int[] water = findNearbyWater();
		// if no water around
		if (water == null) {
			// well...
			stepRand();
			return;
		}
		// Which step toward goal?
		direction = PathFinder.approach(x, y, water[0], water[1], world.getWidth(), world.getHeight());
		moveTo(direction);
		drink();
	}
	

	/**************** Trying to reproduce *******************/
	protected abstract void spawn();
	
	
	/**************** Step through iterations *******************/
	@Override
	public void step()
	{
		// act once every 10 iterations
		if ( World.getIteration() % 10 != 0 ) return;
		// do not move if dead
		if (!isAlive()) return;
		
		if (world.getTemperatureCellValue(x, y) > 80) {
			// roasted
			hp = -1;
			return;
		}
		// If starved or dehydrated, lose hp
		if (hunger>=600||thirst>=600) hp--;
		// else use up food and water. Can regenerate hp
		else {
			// Health regen consuming energy
			hunger+=2;
			thirst++;
			if (world.getTemperatureCellValue(x, y) >= 40) thirst++;	// more dehydrated if hot
			if (hp < 125) hp++;
			// Reproduction based on current health
			if (Math.random() < Math.pow(10, ((hp/25.0) - 5))) {
				spawn();
			}
		}
		
		// take action
		if (World.morning()) dayTimeStep();
		else nightTimeStep();
	}
	
	protected abstract void dayTimeStep();
	
	protected abstract void nightTimeStep();	

    public abstract void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight);
}
