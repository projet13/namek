package Agent;
import javax.media.opengl.GL2;
import mathtool.PathFinder;
import objects.UniqueDynamicObject;
import worlds.World;

public class Prey extends Agent {
	public Prey ( int __x , int __y, World __world )
	{
		super(__x,__y,__world);
	}
	
	//Si se trouve devant un arbre
	private int[] findNearbyTree() {
		// Find in this proximity
		//			*
		//		*	*	*
		//	*	*	me	*	*
		//		*	*	*
		//			*
		int dx = world.getWidth();
		int dy = world.getHeight();
		int[][] toFind = new int[][] {{x, y},
			  {(x+1)%dx, y}, {x, (y+1)%dy}, {(x-1+dx)%dx, y}, {x, (y-1+dy)%dy},
			  {(x+2)%dx, y}, {x, (y+2)%dy}, {(x-2+dx)%dx, y}, {x, (y-2+dy)%dy},
			  {(x+1)%dx, (y+1)%dy}, {(x+1)%dx, (y-1+dy)%dy}, {(x-1+dx)%dx, (y-1+dy)%dy}, {(x-1+dx)%dx, (y+1)%dy}
			 };
		for (int[] coord : toFind) {
			if (world.getForestCellValue(coord[0], coord[1]) == 1)
				return coord;
		}
		return null;
	}
	
	//manger, se nourrire
	private void graze() {
		if (world.getForestCellValue(x, y) == 1) {
			hunger=0;
		}
	}
	
	// if grazed, return true
	private boolean tryGraze() {
		int[] tree = findNearbyTree();
		// Fail to find
		if (tree == null) {
			return false;
		}
		// Which step toward goal?
		int direction = PathFinder.approach(x, y, tree[0], tree[1], world.getWidth(), world.getHeight());
		// move there
		moveTo(direction);
		// eat
		graze();
		// eaten tree disappears. BUG. IT DOESNT WORKS. HELP.
		world.setForestCellValue(x, y, 0);
		return true;
	}
	
	private Predator findNearbyPredator() {
		Predator threat = null;
		int min_distance = Integer.MAX_VALUE;
		int distance;
		for (UniqueDynamicObject a : world.getUniqueDynamicObjects()) {
			if (!(a instanceof Predator)) continue;
			Predator predator = (Predator) a;
			if (!predator.isAlive()) continue;
			distance = PathFinder.distance(x, y, predator.x(), predator.y(), world.getWidth(), world.getHeight());
			if (distance > 3) continue;	// Eye-sight 3
			if (distance < min_distance) {
				min_distance = distance;
				threat = predator;
			}
		}
		return threat;
	}

	private void flee(Predator threat) {
		// fuire du danger !
		// Which step to go away from threat?
		direction = PathFinder.approach(threat.x(), threat.y(), x, y, world.getWidth(), world.getHeight());
		moveTo(direction);
	}
	
	protected void dayTimeStep() {
		// If predator nearby, flee
		Predator threat = findNearbyPredator();
		if (threat != null) {
			flee(threat);
			return;
		}//
		
		// If hungry, try find tree and eat leaves
		if (hunger > 100) {
			boolean grazed = tryGraze();
			if (grazed) return;
		}
		// If thirsty, try find water and drink
		if (thirst > 150) {
			tryDrink();
		}
		// Make sheep sound
		if (Math.random() < 0.0005) {
			world.sd.playSound("audio/sheep.wav");
		}
		// If fine, move randomly
		super.stepRand();
	}
	
	protected void nightTimeStep() {

		// If predator nearby, flee
		Predator threat = findNearbyPredator();
		if (threat != null) {
			flee(threat);
			return;
		}
		
		// clumps up with other preys that are close for mutual protection 
		// find a friend
		Prey friend = null;
		for (UniqueDynamicObject o : world.getUniqueDynamicObjects()) {
			if (!(o instanceof Prey)) continue;
			if (o == this) continue;
			Prey prey = (Prey) o;
			if (!prey.isAlive()) continue;
			if (Math.abs(prey.x() - x) > 6) continue;
			if (Math.abs(prey.y() - y) > 6) continue;
			friend = prey;
			break;
		}
		// approach friend
		if (friend != null) {
			direction = PathFinder.approach(x, y, friend.x(), friend.y(), world.getWidth(), world.getHeight());
			moveTo(direction);
		}
		// no nearby friends
		else {
			stepRand();
		}

		// Make sleeping sound
		if (Math.random() < 0.0005) {
			world.sd.playSound("audio/sleeping.wav");
		}
	}
	
	protected void spawn() {
		// se reproduire
		// la naissance d une nouvelle prey se traduit par 
		// redonner la vie a une prey deja morte
		for (UniqueDynamicObject o : world.getUniqueDynamicObjects()) {
			if (!(o instanceof Prey)) continue;
			Prey offspring = (Prey) o;
			if (offspring.isAlive()) continue;
			if (!canGo((x+1)%world.getWidth(), (y+1)%world.getHeight())) continue;
			offspring.hp = 75;
			offspring.x = (x+1)%world.getWidth();
			offspring.y = (y+1)%world.getHeight();
			return;
		}
	}
	
    public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight){
    	// do not display if dead
    	if (isAlive()) {
           	int x2 = (x-(offsetCA_x%myWorld.getWidth()));
        	if ( x2 < 0) x2+=myWorld.getWidth();
        	int y2 = (y-(offsetCA_y%myWorld.getHeight()));
        	if ( y2 < 0) y2+=myWorld.getHeight();

        	float height = Math.max ( 0 , (float)myWorld.getCellHeight(x, y) );

        	float a = 0.2f; // la base down
            float b = 0.2f; // le haut  up
            float z0 = 0;
            float c = 0.2f;
            float dd = 0.7f;
            // legs Black
             	gl.glColor3f(0f,0f,0f); // black
	            // patte 1 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 2 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 3 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 4 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
            
	        a = 0.2f;
            b = 0.2f;
            z0 += c;
            c =1f ;
            // legs Brown
             	gl.glColor3f(0.8f,0.6f,0.4f); // brown 
	            // patte 1 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 2 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 3 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 4 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);


            a = 0.4f;
            b = 0.4f;
            z0 += c;
            c =1f ;
	        // legs white
             	gl.glColor3f(0.85f,0.85f,0.85f);
	            // patte 1 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 2 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 3 
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX+lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*b, +dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( -dd*lenX + offset+x2*stepX-lenX*a, +dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // patte 4 
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); // a terre (pas d'height*normalizeHeight + z0)
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX+lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*b, -dd*lenY + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( +dd*lenX + offset+x2*stepX-lenX*a, -dd*lenY + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	        a = 1.1f;
            b = 1.1f;
            z0 += c;
            c =1.8f;
            // Body
             	gl.glColor3f(0.95f,0.95f,0.95f);
	            // Sides
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);  
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // Top
	            gl.glColor3f(0.90f,0.90f,0.90f);
	            gl.glVertex3f( offset+x2*stepX-lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX-lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*b, offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( offset+x2*stepX+lenX*a, offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);


	        a = 0.7f;
            b = 0.7f;
            z0 += 0.5f*c;
            dd = 1f;
            c =2f;
            float ddx = dd*lenX, ddy = -dd*lenY;
            switch (direction) {
            	case 1 :
            		ddy = 0;
            		break;
            	case 2 :
            		ddx = 0;
            		break;
            	case 3 :
            		ddy = 0;
            		ddx *= -1;
            		break;
            	case 4:
            		ddx = 0;
            		ddy *= -1;
            		break;
            	default:
            		ddy = 0;
            		break;
            }
	        // Head
        		// Sides
        		gl.glColor3f(0.61f,0.4f,0.12f); // brown 
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            
	            
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0); 
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);

	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY+lenY*a, height*normalizeHeight + z0);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY-lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0);

	            // Top
            	gl.glColor3f(0.9f,0.9f,0.9f); // white
            	if (hunger > 100 && System.currentTimeMillis()/500 % 2 == 0) gl.glColor3f(1.0f,0.5f,0.0f);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX-lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*b, ddy + offset+y2*stepY+lenY*b, height*normalizeHeight + z0 + c);
	            gl.glVertex3f( ddx + offset+x2*stepX+lenX*a, ddy + offset+y2*stepY-lenY*a, height*normalizeHeight + z0 + c);
	            
    	}
    }
}
