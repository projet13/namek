package Agent;
import javax.media.opengl.GL2;

import objects.UniqueDynamicObject;
import worlds.World;

public class Fish extends Agent {
	public Fish ( int __x , int __y, World __world )
	{
		super(__x,__y,__world);
		changeDir();
	}

	/* movements */
	
	@Override
	public boolean canGo(int x, int y) {
		return world.getForestCellValue(x, y) == -1 ;
	}
	
	// never stays still
	@Override
	protected void changeDir() {
		direction = (int) (Math.random() * 8 + 1);
	}
	
	@Override
	// fishes can go diagonally
	protected void moveTo(int dest, int attemptsLeft) {
		if (attemptsLeft <= 0) return;
		boolean succeed;
		switch (dest) {
		case 0:
			return;
		case 1:
			succeed = goRight();
			break;
		case 2:
			succeed = goUp();
			break;
		case 3:
			succeed = goLeft();
			break;
		case 4:
			succeed = goDown();
			break;
		case 5:
			succeed = goRight() && goUp();
			break;
		case 6:
			succeed = goUp() && goLeft();
			break;
		case 7:
			succeed = goLeft() && goDown();
			break;
		case 8:
			succeed = goDown() && goRight();
			break;
		default:
			succeed = false;
		}
		if (!succeed) {
			// If stuck, attempt another direction
			changeDir();
			moveTo(direction, attemptsLeft - 1);
		}
	}

	// this is just like Agent.stepRand(), except no direction change
	protected void trySwimStraight() {
		moveTo(direction);
	}
	
	protected void dayTimeStep() {
		// the fish can feed on sea creature under water that we can't see
		hunger = 0;
		thirst = 0;
		
		trySwimStraight();
	}
	
	protected void nightTimeStep() {
		// moves around more slowly
		if (Math.random() < 0.5) trySwimStraight();
	}

	public void displayUniqueObject(World myWorld, GL2 gl, int offsetCA_x, int offsetCA_y, float offset, float stepX, float stepY, float lenX, float lenY, float normalizeHeight){

           	int x2 = (x-(offsetCA_x%myWorld.getWidth()));
        	if ( x2 < 0) x2+=myWorld.getWidth();
        	int y2 = (y-(offsetCA_y%myWorld.getHeight()));
        	if ( y2 < 0) y2+=myWorld.getHeight();
        	
        	float height = Math.max ( 0 , (float)myWorld.getCellHeight(x, y) );

            gl.glColor3f(0.47f,0.44f,0.40f); //GRAY 0 (darker)
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, height*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, height*normalizeHeight);
            gl.glColor3f(0.58f,0.55f,0.50f); //GRAY 1
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, height*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, height*normalizeHeight);
            gl.glColor3f(0.70f,0.65f,0.60f); //GRAY 
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY-lenY, height*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX+lenX, offset+y2*stepY+lenY, height*normalizeHeight);
            gl.glColor3f(0.75f,0.75f,0.75f); //GRAY 4 (light)
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY+lenY, height*normalizeHeight);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX, offset+y2*stepY, height*normalizeHeight + 4.f);
            gl.glVertex3f( offset+x2*stepX-lenX, offset+y2*stepY-lenY, height*normalizeHeight);

            
    	}


	@Override
	protected void spawn() {
		// se reproduire
		// la naissance d une nouvelle prey se traduit par 
		// redonner la vie a une fish deja morte
		for (UniqueDynamicObject o : world.getUniqueDynamicObjects()) {
			if (!(o instanceof Fish)) continue;
			Fish offspring = (Fish) o;
			if (offspring.isAlive()) continue;	// can't ressurect the living
			if (!canGo((x+1)%world.getWidth(), (y+1)%world.getHeight())) continue;
			offspring.hp = 75;
			offspring.x = (x+1)%world.getWidth();
			offspring.y = (y+1)%world.getHeight();
			return;
		}
	}
}



	
	
    

