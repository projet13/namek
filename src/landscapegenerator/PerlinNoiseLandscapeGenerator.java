// ### WORLD OF CELLS ### 
// created by nicolas.bredeche(at)upmc.fr
// date of creation: 2013-1-12

package landscapegenerator;

import mathtool.PrimeNumber;

public class PerlinNoiseLandscapeGenerator {
	
	// output range (-1.0, 1.0)
	private static double intNoise(int x, int y, int[] set) {
		int n = x + y * set[0] + 1;
		n = (n << 13) ^ n;
		n = (n * (n * n * set[1] + set[2]) + set[3]) & 2147483647;
		return ( 1.0 - n / 1073741824.0);
	}
	
	private static double cosineInterpolate(double a, double b, double x) {
		double ft = x * 3.1415927;
		double f = (1 - Math.cos(ft)) * 0.5;

		return  a*(1-f) + b*f;
	}
	
	private static double perlinNoise(double x, double y, int i, int[] set) {
		int integerX = (int) x;
		double fractionX = x - integerX;
		int integerY = (int) y;
		double fractionY = y - integerY;
		
		int nextX = (integerX + 1) % (int) Math.pow(2, i);
		int nextY = (integerY + 1) % (int) Math.pow(2, i);
		
		double v1 = intNoise(integerX, integerY, set);
		double v2 = intNoise(nextX, integerY, set);
		double v3 = intNoise(integerX, nextY, set);
		double v4 = intNoise(nextX, nextY, set);
		
		double i1 = cosineInterpolate(v1, v2, fractionX);
		double i2 = cosineInterpolate(v3, v4, fractionX);
		
		return cosineInterpolate(i1, i2, fractionY);
	}

    public static double[][] generatePerlinNoiseLandscape ( int dxView, int dyView, double scaling, double landscapeAltitudeRatio, int perlinLayerCount )
    {
    	double landscape[][] = new double[dxView][dyView];

    	// A ECRIRE ! 
    	// cf. http://www.arendpeter.com/Perlin_Noise.html pour une explication (old website down)

    	double[][][] layers = new double[perlinLayerCount][dxView][dyView];
    	
    	// Used to create random number generator functions
    	int [][] randomSets = new int[perlinLayerCount][4];
    	
    	// Initialize random sets
    	for (int i = 0; i < perlinLayerCount; i++) {
    		randomSets[i][0] = PrimeNumber.getRandom();
    		randomSets[i][1] = PrimeNumber.getRandom();
    		randomSets[i][2] = PrimeNumber.getRandom();
    		randomSets[i][3] = PrimeNumber.getRandom();
    	}
    	
    	// Initialize layers
    	int frequency = 1;
    	for (int i = 0; i < perlinLayerCount; i++) {
    		for (int x = 0; x < dxView; x++) {
    			for (int y = 0; y < dyView; y++) {
    				layers[i][x][y] = perlinNoise(x * frequency / (double)dxView, y * frequency / (double)dyView, i, randomSets[i]);
    			}
    		}
    		frequency *= 2;
    	}
    	
    	// Add layers up
    	double amplitude = 1;
    	// Hard-coded persistence choice
    	double persistence = 0.65;
    	for (int i = 0; i < perlinLayerCount; i++) {
    		for (int x = 0; x < dxView; x++) {
    			for (int y = 0; y < dyView; y++) {
    				landscape[x][y] += layers[i][x][y] * amplitude;
    			}
    		}
    		amplitude *= persistence;
    	}

    	// scaling and polishing
    	landscape = LandscapeToolbox.scaleAndCenter(landscape, scaling, landscapeAltitudeRatio);
    	landscape = LandscapeToolbox.smoothLandscape(landscape);
    	
		return landscape;
    }
}
